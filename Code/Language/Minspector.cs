#if UNITY_EDITOR
using UnityEngine;
using UnityEngine.AI;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using System.Collections.Generic;
using System;
using static M.Property;
using static M.System;
using static M.Role;

namespace M
{
	public enum System
	{
		Physics, Animation, PathFinding, Rendering, Audio,

		Shoot
	}

	public enum Property
	{
		position, rotation, scale,
		kinematic, mass, angularMass, velocity, angularVelocity,
		boxCenter, extents,
		sphereCenter, radius,
		mesh, material,
		nearPlane, farPlane, fieldOfView, perspective,
		display, viewport, background,
		emission, cookie, cookieSize, intensity, bounceIntensity,
		range, spotAngle,
		animatorController,
		destination, traversableAreas, maxSpeed, maxAngularSpeed, maxAcceleration,
		audioClip, volume, pitch, loop, mute,
		screenPosition,
		text, font, textMaterial,
		image, imageMaterial,

		lastTimeShot, timerShoot,
	}

	public enum Role
	{
		Body, BoxCollider, SphereCollider,
		Animator,
		Agent,
		Mesh, Camera, Light, Text, Image,
		Source,

		weapon
	}

	public class Minspector : EditorWindow
	{
		public static Minspector Instance;

		List<GameObject> selection;
		public HashSet<string> hiddenComponents = new HashSet<string>();
		Dictionary<GameObject, List<Action<GameObject>>> updates = new Dictionary<GameObject, List<Action<GameObject>>>();

		ScrollView components;
		TextField filterBox;
		Toggle showHidden;
		VisualElement addView;
		PopupField<System> system;
		PopupField<Role> role;


		Dictionary<System, Dictionary<Role,List<Property>>> systems = new Dictionary<System, Dictionary<Role,List<Property>>>
		{
			{
				System.Physics, new Dictionary<Role,List<Property>>
				{
					{ Body, new List<Property>{Property.position, rotation, kinematic, mass, angularMass, velocity, angularVelocity} },
					{ Role.BoxCollider, new List<Property>{Property.position, rotation, scale, boxCenter, extents}},
					{ Role.SphereCollider, new List<Property>{Property.position, rotation, scale, sphereCenter, radius}},
				}
			},
			{
				System.Animation, new Dictionary<Role,List<Property>>
				{
					{ Role.Animator, new List<Property>{animatorController}},
				}
			},
			{
				PathFinding, new Dictionary<Role,List<Property>>
				{
					{ Agent, new List<Property>{Property.position, rotation, scale, destination, traversableAreas, maxSpeed, maxAngularSpeed, maxAcceleration}},
				}
			},
			{
				Rendering, new Dictionary<Role,List<Property>>
				{
					{ Role.Mesh, new List<Property>{Property.position, rotation, scale, mesh, material}},
					{ Role.Camera, new List<Property>{Property.position, rotation, nearPlane, farPlane, fieldOfView, perspective, display, viewport, background}},
					{ Role.Light, new List<Property>{Property.position, rotation, emission, cookie, cookieSize, intensity, bounceIntensity, range, spotAngle}},
					{ Role.Text, new List<Property>{screenPosition, text, font, textMaterial}},
					{ Role.Image, new List<Property>{screenPosition, image, imageMaterial}},
				}
			},
			{
				Audio, new Dictionary<Role,List<Property>>
				{
					{ Source, new List<Property>{Property.position, audioClip, volume, pitch, loop, mute}}
				}
			},
			{
				Shoot, new Dictionary<Role,List<Property>>
				{
					{ weapon, new List<Property>{Property.lastTimeShot, Property.timerShoot}}
				}
			}
		};

		Dictionary<Property,Tuple<Type,Type,String>> data = new Dictionary<Property,Tuple<Type,Type,String>>
		{
			[Property.position] = Tuple.Create(typeof(Transform), typeof(Vector3), "m_LocalPosition"),
			[rotation] = Tuple.Create(typeof(Transform), typeof(Vector3), "m_LocalEulerAnglesHint"),
			[scale] = Tuple.Create(typeof(Transform), typeof(Vector3), "m_LocalScale"),

			[kinematic] = Tuple.Create(typeof(Rigidbody), typeof(Boolean), "m_IsKinematic"),
			[mass] = Tuple.Create(typeof(Rigidbody), typeof(Single), "m_Mass"),
			[angularMass] = Tuple.Create(typeof(Rigidbody), typeof(Vector3), ""),
			[velocity] = Tuple.Create(typeof(Rigidbody), typeof(Vector3), ""),
			[angularVelocity] = Tuple.Create(typeof(Rigidbody), typeof(Vector3), ""),

			[boxCenter] = Tuple.Create(typeof(BoxCollider), typeof(Vector3), "m_Center"),
			[extents] = Tuple.Create(typeof(BoxCollider), typeof(Vector3), "m_Size"),

			[sphereCenter] = Tuple.Create(typeof(SphereCollider), typeof(Vector3), "m_Center"),
			[radius] = Tuple.Create(typeof(SphereCollider), typeof(Single), "m_Radius"),

			[mesh] = Tuple.Create(typeof(SkinnedMeshRenderer), typeof(Mesh), "m_Mesh"),
			[material] = Tuple.Create(typeof(SkinnedMeshRenderer), typeof(Material), "m_Materials.Array.data[0]"),

			[nearPlane] = Tuple.Create(typeof(Camera), typeof(Single), "near clip plane"),
			[farPlane] = Tuple.Create(typeof(Camera), typeof(Single), "far clip plane"),
			[fieldOfView] = Tuple.Create(typeof(Camera), typeof(Single), "field of view"),
			[perspective] = Tuple.Create(typeof(Camera), typeof(Boolean), ""),

			[display] = Tuple.Create(typeof(Camera), typeof(Single), "m_TargetDisplay"),
			[viewport] = Tuple.Create(typeof(Camera), typeof(Rect), "m_NormalizedViewPortRect"),
			[background] = Tuple.Create(typeof(Camera), typeof(Color), "m_BackGroundColor"),

			[emission] = Tuple.Create(typeof(Light), typeof(Color), "m_Color"),
			[cookie] = Tuple.Create(typeof(Light), typeof(Texture), "m_Cookie"),
			[cookieSize] = Tuple.Create(typeof(Light), typeof(Single), "m_CookieSize"),
			[intensity] = Tuple.Create(typeof(Light), typeof(Single), "m_Intensity"),
			[bounceIntensity] = Tuple.Create(typeof(Light), typeof(Single), "m_BounceIntensity"),

			[range] = Tuple.Create(typeof(Light), typeof(Single), "m_Range"),
			[spotAngle] = Tuple.Create(typeof(Light), typeof(Single), "m_SpotAngle"),

			[animatorController] = Tuple.Create(typeof(Animator), typeof(RuntimeAnimatorController), "m_Controller"),

			[destination] = Tuple.Create(typeof(NavMeshAgent), typeof(Vector3), ""),
			[traversableAreas] = Tuple.Create(typeof(NavMeshAgent), typeof(Single), "m_WalkableMask"),
			[maxSpeed] = Tuple.Create(typeof(NavMeshAgent), typeof(Single), "m_Speed"),
			[maxAngularSpeed] = Tuple.Create(typeof(NavMeshAgent), typeof(Single), "m_AngularSpeed"),
			[maxAcceleration] = Tuple.Create(typeof(NavMeshAgent), typeof(Single), "m_Acceleration"),

			[audioClip] = Tuple.Create(typeof(AudioSource), typeof(AudioClip), "m_audioClip"),
			[volume] = Tuple.Create(typeof(AudioSource), typeof(Single), "m_Volume"),
			[pitch] = Tuple.Create(typeof(AudioSource), typeof(Single), "m_Pitch"),
			[loop] = Tuple.Create(typeof(AudioSource), typeof(Boolean), "Loop"),
			[mute] = Tuple.Create(typeof(AudioSource), typeof(Boolean), "Mute"),

			[screenPosition] = Tuple.Create(typeof(RectTransform), typeof(Rect), ""),

			[text] = Tuple.Create(typeof(UnityEngine.UI.Text), typeof(String), "m_Text"),
			[font] = Tuple.Create(typeof(UnityEngine.UI.Text), typeof(Font), "m_FontData.m_Font"),
			[textMaterial] = Tuple.Create(typeof(UnityEngine.UI.Text), typeof(Material), "m_Material"),

			[image] = Tuple.Create(typeof(UnityEngine.UI.RawImage), typeof(Texture), "m_Texture"),
			[imageMaterial] = Tuple.Create(typeof(UnityEngine.UI.RawImage), typeof(Material), "m_Material"),

			[Property.lastTimeShot] = Tuple.Create(typeof(M.lastTimeShot), typeof(Single), "Value"),
			[Property.timerShoot] = Tuple.Create(typeof(M.timerShoot), typeof(Single), "Value"),
		};

		Dictionary<Type,Func<GameObject, Type, Type, string, string, VisualElement, bool, object>> propertyFields;

		[MenuItem("Window/M/Minspector")]
		public static void ShowWindow()
		{
			GetWindow<Minspector>();
		}

		public void Update()
		{
			if (selection == null)
			{
				selection = new List<GameObject>();
			}
			foreach (GameObject go in selection)
			{
				if (updates.ContainsKey(go))
				{
					foreach (var update in updates[go])
					{
						update(go);
					}
				}
			}
		}

		public void OnSelectionChange()
		{
			selection.Clear();
			foreach (var selected in Selection.GetFiltered(typeof(GameObject), SelectionMode.Editable))
			{
				selection.Add((GameObject) selected);
			}
			Render();
		}

		public void OnEnable()
		{
			Instance = this;

			propertyFields = new Dictionary<Type, Func<GameObject, Type, Type, string, string, VisualElement, bool, object>>
			{
				[typeof(Boolean)] = PropertyField<Toggle, Boolean>,
				[typeof(Single)] = PropertyField<FloatField, Single>,
				[typeof(Vector2)] = PropertyField<Vector2Field, Vector2>,
				[typeof(Vector3)] = PropertyField<Vector3Field, Vector3>,
				[typeof(Quaternion)] = PropertyField<Vector4Field, Vector4>,
				[typeof(String)] = PropertyField<TextField, String>,
				[typeof(Color)] = PropertyField<ColorField, Color>,
				[typeof(AnimationCurve)] = PropertyField<CurveField, AnimationCurve>,
				[typeof(Material)] = PropertyField<ObjectField, UnityEngine.Object>,
				[typeof(Mesh)] = PropertyField<ObjectField, UnityEngine.Object>,
				[typeof(Font)] = PropertyField<ObjectField, UnityEngine.Object>,
				[typeof(Texture2D)] = PropertyField<ObjectField, UnityEngine.Object>,
				[typeof(AudioClip)] = PropertyField<ObjectField, UnityEngine.Object>,
				[typeof(RuntimeAnimatorController)] = PropertyField<ObjectField, UnityEngine.Object>,
				[typeof(Rect)] = PropertyField<RectField, Rect>,
				[typeof(Texture)] = PropertyField<ObjectField, UnityEngine.Object>,
				[typeof(GameObject)] = PropertyField<ObjectField, UnityEngine.Object>,
			};

			AddSpace(rootVisualElement);
			rootVisualElement.Add(FilterView());
			rootVisualElement.Add(ComponentsView());
			AddSpace(rootVisualElement);
			rootVisualElement.Add(AddView());
			AddSpace(rootVisualElement);
		}

		private VisualElement FilterView()
		{
			var filterView = new VisualElement();

			filterBox = new TextField("Filter");
			showHidden = new Toggle();

			showHidden.RegisterCallback((ChangeEvent<bool> e) => Render());
			filterBox.RegisterCallback((ChangeEvent<bool> e) => Render());
			showHidden.style.color = Color.red;

			filterView.style.flexDirection = FlexDirection.Row;
			filterBox.style.flexGrow = new StyleFloat(1);
			filterView.Add(filterBox);
			filterView.Add(showHidden);

			return filterView;
		}

		private VisualElement ComponentsView()
		{
			components = new ScrollView();

			return components;
		}

		private VisualElement AddView()
		{
			addView = new VisualElement();
			addView.style.flexDirection = FlexDirection.Row;

			var systemList = new List<System>();
			foreach (var system in Enum.GetValues(typeof(System)))
			{
				systemList.Add((System)system);
			}

			system = new PopupField<System>(systemList, systemList[0]){ label = "Participate in "};

			var roles = new List<Role>();
			foreach (var role in systems[system.value].Keys)
			{
				roles.Add(role);
			}

			role = new PopupField<Role>(roles, roles[0]){ label = "as"};
			var add = new Button(AddRole){ text = "Add role"};

			system.labelElement.style.minWidth = new StyleLength(50);
			role.labelElement.style.minWidth = new StyleLength(30);
			add.style.flexGrow = new StyleFloat(1);

			system.RegisterCallback<ChangeEvent<System>>(SystemChanged);

			addView.Add(system);
			addView.Add(role);
			addView.Add(add);

			return addView;
		}


		private void SystemChanged(ChangeEvent<System> n)
		{
			addView.Remove(role);

			var selectedSystem = n.newValue;

			var roles = new List<Role>();
			foreach (var role in systems[selectedSystem].Keys)
			{
				roles.Add(role);
			}

			role = new PopupField<Role>(roles, roles[0]){ label = "as" };
			role.labelElement.style.minWidth = new StyleLength(30);
			addView.Insert(1, role);
		}

		private void AddRole()
		{
			var selectedSystem = system.value;
			var selectedRole = role.value;

			var properties = systems[selectedSystem][selectedRole];

			var involvedComponents = new HashSet<Type>();
			foreach (var property in properties)
			{
				var component = data[property].Item1;
				involvedComponents.Add(component);
			}

			foreach (GameObject go in selection)
			{
				foreach (var component in involvedComponents)
				{
					if (go.GetComponent(component) == null)
					{
						go.AddComponent(component);
					}
				}
			}

			Render();
		}


		public void Hide(Type component)
		{
			hiddenComponents.Add(component.Name);
			Render();
		}

		public void Unhide(Type component)
		{
			hiddenComponents.Remove(component.Name);
			Render();
		}

		public void Remove(Component component)
		{
			GameObject.DestroyImmediate(component);
			Render();
		}

		public void Render()
		{
			if (components == null) return;

			components.Clear();

			AddSpace(components);

			foreach (GameObject go in selection)
			{
				foreach (var system in systems)
				{
					var validRoles = new Dictionary<Role,List<Property>>();

					foreach (var role in system.Value)
					{
						var involvedComponents = new HashSet<Type>();
						foreach (var property in role.Value)
						{
							involvedComponents.Add(data[property].Item1);
						}

						var validRole = true;
						foreach (var component in involvedComponents)
						{
							if (go.GetComponent(component) == null)
							{
								validRole = false;
								break;
							}
						}

						if (validRole)
						{
							validRoles.Add(role.Key, role.Value);
						}
					}

					if (validRoles.Count > 0)
					{
						var foldout = new Foldout(){ text = system.Key.ToString(), value = false };
						components.Add(foldout);

						foreach (var role in validRoles)
						{
							var roleFoldout = new Foldout(){ text = role.Key.ToString(), value = false };
							roleFoldout.style.marginLeft = new StyleLength(15);
							foldout.Add(roleFoldout);

							foreach (var property in role.Value)
							{
								var d = data[property];
								var componentType = d.Item1;
								var propertyType = d.Item2;
								var objectType = d.Item2;
								var binding = d.Item3;
								var label = property.ToString();
								var parent = roleFoldout;
								var fieldFunction = propertyFields[propertyType];
								var disabled = binding == "";

								var propertyField = fieldFunction(go, componentType, objectType, binding, label, parent, disabled);

								if (binding == "")
								{
									if (!updates.ContainsKey(go))
									{
										updates.Add(go, new List<Action<GameObject>>());
									}

									switch (property)
									{
										case velocity:
											updates[go].Add(go => ((Vector3Field)propertyField).value = go.GetComponent<Rigidbody>().velocity);
										break;
										case angularVelocity:
											updates[go].Add(go => ((Vector3Field)propertyField).value = go.GetComponent<Rigidbody>().angularVelocity);
										break;
										case angularMass:
											updates[go].Add(go => ((Vector3Field)propertyField).value = go.GetComponent<Rigidbody>().inertiaTensor);
										break;
										case perspective:
											updates[go].Add(go => ((Toggle)propertyField).value = !go.GetComponent<Camera>().orthographic);
										break;
										case destination:
											updates[go].Add(go => ((Vector3Field)propertyField).value = go.GetComponent<NavMeshAgent>().destination);
										break;
										case screenPosition:
											updates[go].Add(go =>
											{
												var rectTransform = go.GetComponent<RectTransform>();
												var anchorMin = rectTransform.anchorMin;
												var anchorMax = rectTransform.anchorMax;
												var rectangle = new Rect(anchorMin.x, anchorMin.y, anchorMax.x - anchorMin.x, anchorMax.y - anchorMin.y);
												((RectField)propertyField).value = rectangle;
											});
										break;
									}
								}
							}
						}
					}
				}
			}
		}

		private void AddSpace(VisualElement parent)
		{
			var space = new VisualElement();
			space.style.height = new StyleLength(15);
			parent.Add(space);
		}

		private T PropertyField<T,W>(GameObject go, Type type, Type objectType = null, string bindingPath = "Value", string name = null, VisualElement parent = null, bool disabled = false) where T : BaseField<W>, new()
		{
			var label = name == null ? type.Name : name;
			var field = new T();
			field.label = label;

			field.Bind(new SerializedObject(go.GetComponent(type)));
			field.bindingPath = bindingPath;

			if (field is ObjectField)
			{
				var objectField = (ObjectField)Convert.ChangeType(field, typeof(ObjectField));
				objectField.objectType = objectType;
			}

			if (disabled)
			{
				field.SetEnabled(false);
			}

			if (parent == null)
			{
				components.Add(field);
			}
			else
			{
				parent.Add(field);
			}

			return field;
		}
	}
}
#endif

