#if UNITY_EDITOR
using UnityEngine;
using UnityEngine.AI;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;
using System;
namespace M
{
	[InitializeOnLoad]
	public class NoNulls : Editor
	{
		static bool strictMode = true;

		static NoNulls()
		{
			EditorApplication.playModeStateChanged += HandleOnPlayModeChanged;
		}

		static void HandleOnPlayModeChanged(PlayModeStateChange state)
		{
			if (state == PlayModeStateChange.EnteredPlayMode)
			{
				foreach (Transform transform in GameObject.FindObjectsOfType<Transform>())
				{
					var nulls = new List<string>();
					var go = transform.gameObject;

					// Builtin nullable values
					var renderer = go.GetComponent<SkinnedMeshRenderer>();
					var light = go.GetComponent<Light>();
					var audio = go.GetComponent<AudioSource>();
					var animator = go.GetComponent<Animator>();

					if (renderer)
					{
						if (renderer.sharedMesh == null)
						{
							nulls.Add("renderer.mesh");
						}
						if (renderer.materials[0] == null)
						{
							nulls.Add("renderer.material");
						}
					}
					if (light)
					{
						if (light.cookie == null)
						{
							nulls.Add("light.cookie");
						}
					}
					if (audio)
					{
						if (audio.clip == null)
						{
							nulls.Add("audio.clip");
						}
					}
					if (animator)
					{
						if (animator.runtimeAnimatorController == null)
						{
							nulls.Add("animator.controller");
						}
					}

					// Custom components

					var meshino = go.GetComponent<M.meshino>();
					if (meshino && meshino.Value == null)
					{
						nulls.Add("meshino.Value");
					}

					foreach (var error in nulls)
					{
						Debug.Log(go.name + " has null values: " + error);
					}

					if (strictMode)
					{
						EditorApplication.isPlaying = false;
					}
				}
			}
		}
	}
}
#endif

