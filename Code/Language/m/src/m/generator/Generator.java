package m.generator;

import m.model.Game;

public class Generator
{
	Unity unity = new Unity();

	public void generate(Game game, String path)
	{
		IO.setBaseFolder(path);

		unity.generate(game);
	}
}
