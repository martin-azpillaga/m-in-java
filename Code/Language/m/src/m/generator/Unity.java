package m.generator;

import static m.generator.IO.end;
import static m.generator.IO.exists;
import static m.generator.IO.foreach;
import static m.generator.IO.getBaseFolder;
import static m.generator.IO.iff;
import static m.generator.IO.lines;
import static m.generator.IO.readText;
import static m.generator.IO.write;
import static m.generator.IO.writeFile;
import static m.library.symbols.Component.DISPLAY;
import static m.library.symbols.Function.ACTIVATE_ANIMATOR_TRIGGER;
import static m.library.symbols.Function.ACTIVATE_PARAMETER;
import static m.library.symbols.Function.ADD_FORCE;
import static m.library.symbols.Function.ADD_TORQUE;
import static m.library.symbols.Function.DEACTIVATE_PARAMETER;
import static m.library.symbols.Function.PLAY_ANIMATION;
import static m.library.symbols.Function.IN_STATE;
import static m.library.symbols.Function.OVERLAPS;
import static m.library.symbols.Function.PAUSE;
import static m.library.symbols.Function.PLAY;
import static m.library.symbols.Function.PLAY_ONCE;
import static m.library.symbols.Function.STOP;
import static m.library.symbols.Function.VIEWPORT_TO_WORLD;
import static m.library.symbols.Function.WORLD_TO_VIEWPORT;
import static m.library.types.AtomicType.INPUT;
import static m.library.types.AtomicType.NOTHING;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import com.google.gson.Gson;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.EcoreUtil2;

import m.library.Library;
import m.library.types.AtomicType;
import m.library.types.FunctionType;
import m.library.types.Type;
import m.model.Application;
import m.model.Assignment;
import m.model.Binary;
import m.model.BindingBlock;
import m.model.Block;
import m.model.Cell;
import m.model.Delegation;
import m.model.Expression;
import m.model.Function;
import m.model.Game;
import m.model.Statement;
import m.model.Unary;
import m.model.UserFunction;
import m.model.Value;

public class Unity
{
	Game game;
	Library library;

	Set<String> namespaces;
	UserFunction currentFunction;
	HashMap<String,Boolean> variables;
	Deque<HashMap<String,Boolean>> stack;
	HashMap<Application,String> overlapNames;

	static final String[] csharpReserved = new String[]{ "abstract", "as", "base", "bool",	"break", "byte", "case", "catch", "char", "checked", "class", "const", "continue", "decimal", "default", "delegate",	"do", "double", "else", "enum", "event", "explicit", "extern", "false", "finally", "fixed", "float", "for", "foreach", "goto", "if", "implicit", "in", "int", "interface", "internal", "is", "lock", "long", "namespace", "new", "null", "object", "operator", "out", "override", "params", "private", "protected", "public", "readonly", "ref", "return", "sbyte", "sealed", "short", "sizeof", "stackalloc", "static", "string", "struct", "switch", "this", "throw",	"true", "try", "typeof", "uint", "ulong", "unchecked", "unsafe", "ushort",	"using", "virtual", "void", "volatile", "while" };

	public void generate(Game game)
	{
		this.game = game;
		this.library = game.library;
		this.variables = new HashMap<>();
		this.stack = new ArrayDeque<>();
		this.overlapNames = new HashMap<>();
		this.namespaces = new HashSet<>();

		resolvePackages();

		clean(Paths.get(getBaseFolder(), "Assets", "Code").toString().replace("file:", ""));

		// resolveAssembly();

		game.components.entrySet().removeIf(e-> library.getComponent(e.getKey()) != null);

		game.components.forEach((name, type)->
		{
			writeFile("Assets/Code/Components/"+unreserved(name)+".cs",	generateComponent(name, type));
		});

		writeFile("Assets/Code/Components/Momponent.cs", generateMomponent());

		var systems = new ArrayList<UserFunction>();

		for (var function : game.functions)
		{
			var type = function.type;
			if (type == FunctionType.systemType)
			{
				systems.add(function);
				writeFile("Assets/Code/Systems/"+unreserved(function.getName())+".cs", generateSystem(function));
			}
		}

		writeFile("Assets/Code/Systems/SystemRunner.cs", systemRunner(systems));

		writeFile("Assets/Code/Editor/Minspector.cs", generateMinspector(systems));
	}

	private void resolvePackages()
	{
		final var version100 = "1.0.0";

		var needed = new HashMap<String,String>();
		needed.put("com.unity.inputsystem", "1.0.2");
		needed.put("com.unity.mathematics", "1.2.1");

		var file = "Packages/manifest.json";

		if (exists(file))
		{
			var regenerate = false;
			var current = readText(file).toString();
			var manifest = new Gson().fromJson(current, PackageManifest.class);
			var dependencies = manifest.dependencies;

			for (var dependency : needed.entrySet())
			{
				if (!dependencies.containsKey(dependency.getKey()))
				{
					regenerate = true;
					dependencies.put(dependency.getKey(), dependency.getValue());
				}
			}
			if (regenerate)
			{
				var json = new Gson().toJson(manifest);
				writeFile(file, json);
			}
		}
		else
		{
			var map = new HashMap<String,String>();
			for (var dependency : needed.entrySet())
			{
				map.put(dependency.getKey(), dependency.getValue());
			}
			map.put("com.unity.ugui", version100);
			map.put("com.unity.modules.ai", version100);
			map.put("com.unity.modules.androidjni", version100);
			map.put("com.unity.modules.animation", version100);
			map.put("com.unity.modules.assetbundle", version100);
			map.put("com.unity.modules.audio", version100);
			map.put("com.unity.modules.cloth", version100);
			map.put("com.unity.modules.director", version100);
			map.put("com.unity.modules.imageconversion", version100);
			map.put("com.unity.modules.imgui", version100);
			map.put("com.unity.modules.jsonserialize", version100);
			map.put("com.unity.modules.particlesystem", version100);
			map.put("com.unity.modules.physics", version100);
			map.put("com.unity.modules.physics2d", version100);
			map.put("com.unity.modules.screencapture", version100);
			map.put("com.unity.modules.terrain", version100);
			map.put("com.unity.modules.terrainphysics", version100);
			map.put("com.unity.modules.tilemap", version100);
			map.put("com.unity.modules.ui", version100);
			map.put("com.unity.modules.uielements", version100);
			map.put("com.unity.modules.umbra", version100);
			map.put("com.unity.modules.unityanalytics", version100);
			map.put("com.unity.modules.unitywebrequest", version100);
			map.put("com.unity.modules.unitywebrequestassetbundle", version100);
			map.put("com.unity.modules.unitywebrequestaudio", version100);
			map.put("com.unity.modules.unitywebrequesttexture", version100);
			map.put("com.unity.modules.unitywebrequestwww", version100);
			map.put("com.unity.modules.vehicles", version100);
			map.put("com.unity.modules.video", version100);
			map.put("com.unity.modules.vr", version100);
			map.put("com.unity.modules.wind", version100);
			map.put("com.unity.modules.xr", version100);
			var json = new Gson().toJson(new PackageManifest(map));
			writeFile(file, json);
		}
	}

	private void resolveAssembly()
	{
		var file = "Assets/Code/M.asmdef";
		var gson = new Gson();
		var needed = new ArrayList<String>();
		needed.add("Unity.Mathematics");
		needed.add("Unity.InputSystem");

		if (exists(file))
		{
			var regenerate = false;

			var text = readText(file).toString();
			var assembly = gson.fromJson(text, AssemblyDefinition.class);

			var references = assembly.references;

			for (var need : needed)
			{
				if (!references.contains(need))
				{
					regenerate = true;
					references.add(need);
				}
			}

			if (regenerate)
			{
				var json = gson.toJson(assembly);
				writeFile(file, json);
			}
		}
		else
		{
			var assembly = new AssemblyDefinition("M",needed,true);
			var json = gson.toJson(assembly, AssemblyDefinition.class);
			writeFile(file, json);
		}
	}

	private String generateMomponent()
	{
		namespaces.clear();
		namespaces.add("UnityEngine");

		var lines = lines
		(
			"namespace M",
			"{",
				"public abstract class Momponent<T> : MonoBehaviour",
				"{",
					"public T Value;",
				"}",
			"}"
		);

		return write
		(
			foreach(namespaces, n->"using "+n+";"),
			"",
			lines
		);
	}

	private String generateMinspector(List<UserFunction> systems)
	{
		var roles = new HashSet<String>();
		for (var system : systems)
		{
			for (var role : system.roles.keySet())
			{
				roles.add(role);
			}
		}

		namespaces.add("UnityEngine");
		namespaces.add("UnityEngine.AI");
		namespaces.add("UnityEditor");
		namespaces.add("UnityEditor.UIElements");
		namespaces.add("UnityEngine.UIElements");
		namespaces.add("System.Collections.Generic");
		namespaces.add("System");
		namespaces.add("UnityEngine.InputSystem");
		namespaces.add("static M.Property");
		namespaces.add("static M.System");
		namespaces.add("static M.Role");

		var lines = lines
		(
		"namespace M",
		"{",
			"public enum System",
			"{",
				"Physics, Animation, PathFinding, Rendering, Audio,",

				foreach(systems, s->s.getName(), ", "),
			"}",

			"public enum Property",
			"{",
				"position, rotation, scale,",
				"kinematic, mass, angularMass, velocity, angularVelocity,",
				"boxCenter, extents,",
				"sphereCenter, radius,",
				"mesh, material,",
				"nearPlane, farPlane, fieldOfView, perspective,",
				"display, viewport, background,",
				"emission, cookie, cookieSize, intensity, bounceIntensity,",
				"range, spotAngle,",
				"animatorController,",
				"destination, traversableAreas, maxSpeed, maxAngularSpeed, maxAcceleration,",
				"audioClip, volume, pitch, loop, mute,",
				"screenPosition,",
				"text, font, textMaterial,",
				"image, imageMaterial,",

				foreach(game.components.keySet(), c->c, ", "),
			"}",

			"public enum Role",
			"{",
				"Body, BoxCollider, SphereCollider,",
				"Animator,",
				"Agent,",
				"Mesh, Camera, Light, Text, Image,",
				"Source,",

				foreach(roles, r->r, ", "),
			"}",

			"public class Minspector : EditorWindow",
			"{",
				"public static Minspector Instance;",

				"List<GameObject> selection;",
				"public HashSet<string> hiddenComponents = new HashSet<string>();",
				"Dictionary<GameObject, List<Action<GameObject>>> updates = new Dictionary<GameObject, List<Action<GameObject>>>();",

				"ScrollView components;",
				"TextField filterBox;",
				"UnityEngine.UIElements.Toggle showHidden;",
				"VisualElement addView;",
				"PopupField<System> system;",
				"PopupField<Role> role;",
"",

				"Dictionary<System, Dictionary<Role,List<Property>>> systems = new Dictionary<System, Dictionary<Role,List<Property>>>",
				"{",
					"{",
						"System.Physics, new Dictionary<Role,List<Property>>",
						"{",
							"{ Body, new List<Property>{Property.position, rotation, kinematic, mass, angularMass, velocity, angularVelocity} },",
							"{ Role.BoxCollider, new List<Property>{Property.position, rotation, scale, boxCenter, extents}},",
							"{ Role.SphereCollider, new List<Property>{Property.position, rotation, scale, sphereCenter, radius}},",
						"}",
					"},",
					"{",
						"System.Animation, new Dictionary<Role,List<Property>>",
						"{",
							"{ Role.Animator, new List<Property>{animatorController}},",
						"}",
					"},",
					"{",
						"PathFinding, new Dictionary<Role,List<Property>>",
						"{",
							"{ Agent, new List<Property>{Property.position, rotation, scale, destination, traversableAreas, maxSpeed, maxAngularSpeed, maxAcceleration}},",
						"}",
					"},",
					"{",
						"Rendering, new Dictionary<Role,List<Property>>",
						"{",
							"{ Role.Mesh, new List<Property>{Property.position, rotation, scale, mesh, material}},",
							"{ Role.Camera, new List<Property>{Property.position, rotation, nearPlane, farPlane, fieldOfView, perspective, display, viewport, background}},",
							"{ Role.Light, new List<Property>{Property.position, rotation, emission, cookie, cookieSize, intensity, bounceIntensity, range, spotAngle}},",
							"{ Role.Text, new List<Property>{screenPosition, text, font, textMaterial}},",
							"{ Role.Image, new List<Property>{screenPosition, image, imageMaterial}},",
						"}",
					"},",
					"{",
						"Audio, new Dictionary<Role,List<Property>>",
						"{",
							"{ Source, new List<Property>{Property.position, audioClip, volume, pitch, loop, mute}}",
						"}",
					"},",

					foreach(systems, s->lines
					(
						"{",
							"System."+s.getName()+", new Dictionary<Role,List<Property>>",
							"{",
								foreach(s.roles.entrySet(), r->"{ Role."+r.getKey()+", new List<Property>{"+foreach(r.getValue().keySet(), p->"Property."+p, ", ")+"} },"),
							"}",
						"},"
					)),
				"};",

				"Dictionary<Property,Tuple<Type,Type,String>> data = new Dictionary<Property,Tuple<Type,Type,String>>",
				"{",
					"[Property.position] = Tuple.Create(typeof(Transform), typeof(Vector3), \"m_LocalPosition\"),",
					"[rotation] = Tuple.Create(typeof(Transform), typeof(Vector3), \"m_LocalEulerAnglesHint\"),",
					"[scale] = Tuple.Create(typeof(Transform), typeof(Vector3), \"m_LocalScale\"),",

					"[kinematic] = Tuple.Create(typeof(Rigidbody), typeof(Boolean), \"m_IsKinematic\"),",
					"[mass] = Tuple.Create(typeof(Rigidbody), typeof(Single), \"m_Mass\"),",
					"[angularMass] = Tuple.Create(typeof(Rigidbody), typeof(Vector3), \"\"),",
					"[velocity] = Tuple.Create(typeof(Rigidbody), typeof(Vector3), \"\"),",
					"[angularVelocity] = Tuple.Create(typeof(Rigidbody), typeof(Vector3), \"\"),",

					"[boxCenter] = Tuple.Create(typeof(BoxCollider), typeof(Vector3), \"m_Center\"),",
					"[extents] = Tuple.Create(typeof(BoxCollider), typeof(Vector3), \"m_Size\"),",

					"[sphereCenter] = Tuple.Create(typeof(SphereCollider), typeof(Vector3), \"m_Center\"),",
					"[radius] = Tuple.Create(typeof(SphereCollider), typeof(Single), \"m_Radius\"),",

					"[mesh] = Tuple.Create(typeof(MeshFilter), typeof(Mesh), \"m_Mesh\"),",
					"[material] = Tuple.Create(typeof(MeshRenderer), typeof(Material), \"m_Materials.Array.data[0]\"),",

					"[nearPlane] = Tuple.Create(typeof(Camera), typeof(Single), \"near clip plane\"),",
					"[farPlane] = Tuple.Create(typeof(Camera), typeof(Single), \"far clip plane\"),",
					"[fieldOfView] = Tuple.Create(typeof(Camera), typeof(Single), \"field of view\"),",
					"[perspective] = Tuple.Create(typeof(Camera), typeof(Boolean), \"\"),",

					"[display] = Tuple.Create(typeof(Camera), typeof(Single), \"m_TargetDisplay\"),",
					"[viewport] = Tuple.Create(typeof(Camera), typeof(Rect), \"m_NormalizedViewPortRect\"),",
					"[background] = Tuple.Create(typeof(Camera), typeof(Color), \"m_BackGroundColor\"),",

					"[emission] = Tuple.Create(typeof(Light), typeof(Color), \"m_Color\"),",
					"[cookie] = Tuple.Create(typeof(Light), typeof(Texture), \"m_Cookie\"),",
					"[cookieSize] = Tuple.Create(typeof(Light), typeof(Single), \"m_CookieSize\"),",
					"[intensity] = Tuple.Create(typeof(Light), typeof(Single), \"m_Intensity\"),",
					"[bounceIntensity] = Tuple.Create(typeof(Light), typeof(Single), \"m_BounceIntensity\"),",

					"[range] = Tuple.Create(typeof(Light), typeof(Single), \"m_Range\"),",
					"[spotAngle] = Tuple.Create(typeof(Light), typeof(Single), \"m_SpotAngle\"),",

					"[animatorController] = Tuple.Create(typeof(Animator), typeof(RuntimeAnimatorController), \"m_Controller\"),",

					"[destination] = Tuple.Create(typeof(NavMeshAgent), typeof(Vector3), \"\"),",
					"[traversableAreas] = Tuple.Create(typeof(NavMeshAgent), typeof(Single), \"m_WalkableMask\"),",
					"[maxSpeed] = Tuple.Create(typeof(NavMeshAgent), typeof(Single), \"m_Speed\"),",
					"[maxAngularSpeed] = Tuple.Create(typeof(NavMeshAgent), typeof(Single), \"m_AngularSpeed\"),",
					"[maxAcceleration] = Tuple.Create(typeof(NavMeshAgent), typeof(Single), \"m_Acceleration\"),",

					"[audioClip] = Tuple.Create(typeof(AudioSource), typeof(AudioClip), \"m_audioClip\"),",
					"[volume] = Tuple.Create(typeof(AudioSource), typeof(Single), \"m_Volume\"),",
					"[pitch] = Tuple.Create(typeof(AudioSource), typeof(Single), \"m_Pitch\"),",
					"[loop] = Tuple.Create(typeof(AudioSource), typeof(Boolean), \"Loop\"),",
					"[mute] = Tuple.Create(typeof(AudioSource), typeof(Boolean), \"Mute\"),",

					"[screenPosition] = Tuple.Create(typeof(RectTransform), typeof(Rect), \"\"),",

					"[text] = Tuple.Create(typeof(UnityEngine.UI.Text), typeof(String), \"m_Text\"),",
					"[font] = Tuple.Create(typeof(UnityEngine.UI.Text), typeof(Font), \"m_FontData.m_Font\"),",
					"[textMaterial] = Tuple.Create(typeof(UnityEngine.UI.Text), typeof(Material), \"m_Material\"),",

					"[image] = Tuple.Create(typeof(UnityEngine.UI.RawImage), typeof(Texture), \"m_Texture\"),",
					"[imageMaterial] = Tuple.Create(typeof(UnityEngine.UI.RawImage), typeof(Material), \"m_Material\"),",

					foreach(game.components.entrySet(), c->"[Property."+c.getKey()+"] = Tuple.Create(typeof("+component(c.getKey())+"), typeof("+code(c.getValue())+"), \"Value\"),"),
				"};",

				"Dictionary<Type,Func<GameObject, Type, Type, string, string, VisualElement, bool, object>> propertyFields;",

				"[MenuItem(\"Window/M/Minspector\")]",
				"public static void ShowWindow()",
				"{",
					"GetWindow<Minspector>();",
				"}",

				"public void Update()",
				"{",
					"if (selection == null)",
					"{",
						"selection = new List<GameObject>();",
					"}",
					"foreach (GameObject go in selection)",
					"{",
						"if (updates.ContainsKey(go))",
						"{",
							"foreach (var update in updates[go])",
							"{",
								"update(go);",
							"}",
						"}",
					"}",
				"}",

				"public void OnSelectionChange()",
				"{",
					"selection.Clear();",
					"foreach (var selected in Selection.GetFiltered(typeof(GameObject), SelectionMode.Editable))",
					"{",
						"selection.Add((GameObject) selected);",
					"}",
					"Render();",
				"}",

				"public void OnEnable()",
				"{",
					"Instance = this;",

					"propertyFields = new Dictionary<Type, Func<GameObject, Type, Type, string, string, VisualElement, bool, object>>",
					"{",
						"[typeof(Boolean)] = PropertyField<UnityEngine.UIElements.Toggle, Boolean>,",
						"[typeof(Single)] = PropertyField<FloatField, Single>,",
						"[typeof(Vector2)] = PropertyField<Vector2Field, Vector2>,",
						"[typeof(Vector3)] = PropertyField<Vector3Field, Vector3>,",
						"[typeof(Quaternion)] = PropertyField<Vector4Field, Vector4>,",
						"[typeof(String)] = PropertyField<TextField, String>,",
						"[typeof(Color)] = PropertyField<ColorField, Color>,",
						"[typeof(AnimationCurve)] = PropertyField<CurveField, AnimationCurve>,",
						"[typeof(Material)] = PropertyField<ObjectField, UnityEngine.Object>,",
						"[typeof(Mesh)] = PropertyField<ObjectField, UnityEngine.Object>,",
						"[typeof(Font)] = PropertyField<ObjectField, UnityEngine.Object>,",
						"[typeof(Texture2D)] = PropertyField<ObjectField, UnityEngine.Object>,",
						"[typeof(AudioClip)] = PropertyField<ObjectField, UnityEngine.Object>,",
						"[typeof(RuntimeAnimatorController)] = PropertyField<ObjectField, UnityEngine.Object>,",
						"[typeof(Rect)] = PropertyField<RectField, Rect>,",
						"[typeof(Texture)] = PropertyField<ObjectField, UnityEngine.Object>,",
						"[typeof(GameObject)] = PropertyField<ObjectField, UnityEngine.Object>,",
						"[typeof(InputAction)] = PropertyField<ObjectField, UnityEngine.Object>,",
					"};",

					"AddSpace(rootVisualElement);",
					"rootVisualElement.Add(FilterView());",
					"rootVisualElement.Add(ComponentsView());",
					"AddSpace(rootVisualElement);",
					"rootVisualElement.Add(AddView());",
					"AddSpace(rootVisualElement);",
				"}",

				"private VisualElement FilterView()",
				"{",
					"var filterView = new VisualElement();",

					"filterBox = new TextField(\"Filter\");",
					"showHidden = new UnityEngine.UIElements.Toggle();",

					"showHidden.RegisterCallback((ChangeEvent<bool> e) => Render());",
					"filterBox.RegisterCallback((ChangeEvent<bool> e) => Render());",
					"showHidden.style.color = Color.red;",

					"filterView.style.flexDirection = FlexDirection.Row;",
					"filterBox.style.flexGrow = new StyleFloat(1);",
					"filterView.Add(filterBox);",
					"filterView.Add(showHidden);",

					"return filterView;",
				"}",

				"private VisualElement ComponentsView()",
				"{",
					"components = new ScrollView();",

					"return components;",
				"}",

				"private VisualElement AddView()",
				"{",
					"addView = new VisualElement();",
					"addView.style.flexDirection = FlexDirection.Row;",

					"var systemList = new List<System>();",
					"foreach (var system in Enum.GetValues(typeof(System)))",
					"{",
						"systemList.Add((System)system);",
					"}",

					"system = new PopupField<System>(systemList, systemList[0]){ label = \"Participate in \"};",

					"var roles = new List<Role>();",
					"foreach (var role in systems[system.value].Keys)",
					"{",
						"roles.Add(role);",
					"}",

					"role = new PopupField<Role>(roles, roles[0]){ label = \"as\"};",
					"var add = new UnityEngine.UIElements.Button(AddRole){ text = \"Add role\"};",

					"system.labelElement.style.minWidth = new StyleLength(50);",
					"role.labelElement.style.minWidth = new StyleLength(30);",
					"add.style.flexGrow = new StyleFloat(1);",

					"system.RegisterCallback<ChangeEvent<System>>(SystemChanged);",

					"addView.Add(system);",
					"addView.Add(role);",
					"addView.Add(add);",

					"return addView;",
				"}",
"",

				"private void SystemChanged(ChangeEvent<System> n)",
				"{",
					"addView.Remove(role);",

					"var selectedSystem = n.newValue;",

					"var roles = new List<Role>();",
					"foreach (var role in systems[selectedSystem].Keys)",
					"{",
						"roles.Add(role);",
					"}",

					"role = new PopupField<Role>(roles, roles[0]){ label = \"as\" };",
					"role.labelElement.style.minWidth = new StyleLength(30);",
					"addView.Insert(1, role);",
				"}",

				"private void AddRole()",
				"{",
					"var selectedSystem = system.value;",
					"var selectedRole = role.value;",

					"var properties = systems[selectedSystem][selectedRole];",

					"var involvedComponents = new HashSet<Type>();",
					"foreach (var property in properties)",
					"{",
						"var component = data[property].Item1;",
						"involvedComponents.Add(component);",
					"}",

					"foreach (GameObject go in selection)",
					"{",
						"foreach (var component in involvedComponents)",
						"{",
							"if (go.GetComponent(component) == null)",
							"{",
								"go.AddComponent(component);",
							"}",
						"}",
					"}",

					"Render();",
				"}",
				"",
				"public void Hide(Type component)",
				"{",
					"hiddenComponents.Add(component.Name);",
					"Render();",
				"}",
				"",
				"public void Reveal(Type component)",
				"{",
					"hiddenComponents.Remove(component.Name);",
					"Render();",
				"}",
				"",
				"public void Remove(Component component)",
				"{",
					"GameObject.DestroyImmediate(component);",
					"Render();",
				"}",

				"public void Render()",
				"{",
					"if (components == null) return;",

					"components.Clear();",

					"AddSpace(components);",

					"foreach (GameObject go in selection)",
					"{",
						"if (!updates.ContainsKey(go))",
						"{",
							"updates.Add(go, new List<Action<GameObject>>());",
						"}",
						"else",
						"{",
							"updates[go] = new List<Action<GameObject>>();",
						"}",
						"foreach (var system in systems)",
						"{",
							"var validRoles = new Dictionary<Role,List<Property>>();",

							"foreach (var role in system.Value)",
							"{",
								"var involvedComponents = new HashSet<Type>();",
								"foreach (var property in role.Value)",
								"{",
									"involvedComponents.Add(data[property].Item1);",
								"}",

								"var validRole = true;",
								"foreach (var component in involvedComponents)",
								"{",
									"if (go.GetComponent(component) == null)",
									"{",
										"validRole = false;",
										"break;",
									"}",
								"}",

								"if (validRole)",
								"{",
									"validRoles.Add(role.Key, role.Value);",
								"}",
							"}",

							"if (validRoles.Count > 0)",
							"{",
								"var foldout = new Foldout(){ text = system.Key.ToString(), value = false };",
								"components.Add(foldout);",

								"foreach (var role in validRoles)",
								"{",
									"var roleFoldout = new Foldout(){ text = role.Key.ToString(), value = false };",
									"roleFoldout.style.marginLeft = new StyleLength(15);",
									"foldout.Add(roleFoldout);",

									"foreach (var property in role.Value)",
									"{",
										"var d = data[property];",
										"var componentType = d.Item1;",
										"var propertyType = d.Item2;",
										"var objectType = d.Item2;",
										"var binding = d.Item3;",
										"var label = property.ToString();",
										"var parent = roleFoldout;",
										"var fieldFunction = propertyFields[propertyType];",
										"var disabled = binding == \"\" || propertyType == typeof(InputAction);",

										"var propertyField = fieldFunction(go, componentType, objectType, binding, label, parent, disabled);",

										"if (binding == \"\")",
										"{",
											"switch (property)",
											"{",
												"case velocity:",
													"updates[go].Add(go => ((Vector3Field)propertyField).value = go.GetComponent<Rigidbody>().velocity);",
												"break;",
												"case angularVelocity:",
													"updates[go].Add(go => ((Vector3Field)propertyField).value = go.GetComponent<Rigidbody>().angularVelocity);",
												"break;",
												"case angularMass:",
													"updates[go].Add(go => ((Vector3Field)propertyField).value = go.GetComponent<Rigidbody>().inertiaTensor);",
												"break;",
												"case perspective:",
													"updates[go].Add(go => ((UnityEngine.UIElements.Toggle)propertyField).value = !go.GetComponent<Camera>().orthographic);",
												"break;",
												"case destination:",
													"updates[go].Add(go => ((Vector3Field)propertyField).value = go.GetComponent<NavMeshAgent>().destination);",
												"break;",
												"case screenPosition:",
													"updates[go].Add(go =>",
													"{",
														"var rectTransform = go.GetComponent<RectTransform>();",
														"var anchorMin = rectTransform.anchorMin;",
														"var anchorMax = rectTransform.anchorMax;",
														"var rectangle = new Rect(anchorMin.x, anchorMin.y, anchorMax.x - anchorMin.x, anchorMax.y - anchorMin.y);",
														"((RectField)propertyField).value = rectangle;",
													"});",
												"break;",
											"}",
										"}",
									"}",
								"}",
							"}",
						"}",
					"}",
				"}",

				"private void AddSpace(VisualElement parent)",
				"{",
					"var space = new VisualElement();",
					"space.style.height = new StyleLength(15);",
					"parent.Add(space);",
				"}",

				"private T PropertyField<T,W>(GameObject go, Type type, Type objectType, string bindingPath, string name, VisualElement parent, bool disabled) where T : BaseField<W>, new()",
				"{",
					"var label = name;",
					"var field = new T();",
					"field.label = label;",

					"field.Bind(new SerializedObject(go.GetComponent(type)));",
					"field.bindingPath = bindingPath;",

					"if (field is ObjectField)",
					"{",
						"var objectField = (ObjectField)Convert.ChangeType(field, typeof(ObjectField));",
						"objectField.objectType = objectType;",
					"}",

					"if (disabled)",
					"{",
						"field.SetEnabled(false);",
					"}",

					"if (parent == null)",
					"{",
						"components.Add(field);",
					"}",
					"else",
					"{",
						"parent.Add(field);",
					"}",

					"return field;",
				"}",
			"}",
		"}"
		);

		return write
		(
			"#if UNITY_EDITOR",
			foreach(namespaces, n->"using "+n+";"),
			lines,
			"#endif"
		);
	}

	private String generateComponent(String name, Type type)
	{
		namespaces.clear();
		namespaces.add("UnityEngine");

		var lines = lines
		(
			"namespace M",
			"{",
				"[DisallowMultipleComponent]",
				"public class "+unreserved(name)+" : "+(type!=NOTHING ? "Momponent<"+code(type)+">" : "MonoBehaviour"),
				"{",
					iff(type == INPUT),
					"void Awake()",
					"{",
						"if (Value != null)",
						"{",
							"Value.Enable();",
						"}",
					"}",
					end,
				"}",
			"}"
		);

		return write
		(
			foreach(namespaces, n->"using "+n+";"),
			"",
			lines
		);
	}

	private String systemRunner(List<UserFunction> systems)
	{
		return write
		(
			"using UnityEngine;",
			"using System.Collections.Generic;",
			"using UnityEngine.SceneManagement;",
			"",
			"namespace M",
			"{",
				"public interface ISystem",
				"{",
					"void Reset();",
					"void Classify(GameObject go);",
					"void Destroy(GameObject go);",
					"void Run();",
				"}",
				"",
				"public class SystemRunner : MonoBehaviour",
				"{",
					"static List<ISystem> systems = new List<ISystem>{ "+foreach(systems, s->"new "+s.getName()+"()",", ")+" };",
					"",
					"[RuntimeInitializeOnLoadMethod]",
					"static void Initialize()",
					"{",
						"var go = new GameObject(\"System Runner\");",
						"go.AddComponent<SystemRunner>();",
						"Object.DontDestroyOnLoad(go);",
						"DetectAllEntities();",
					"}",
					"",
					"static void DetectAllEntities()",
					"{",
						"for (var i = 0; i < SceneManager.sceneCount; i++)",
						"{",
							"var scene = SceneManager.GetSceneAt(i);",
							"foreach (var root in scene.GetRootGameObjects())",
							"{",
								"DetectRecursive(root.transform);",
							"}",
						"}",
					"}",
					"",
					"static void DetectRecursive(Transform transform)",
					"{",
						"foreach (var system in systems)",
						"{",
							"system.Classify(transform.gameObject);",
						"}",
						"foreach (Transform child in transform)",
						"{",
							"DetectRecursive(child);",
						"}",
					"}",
					"void Awake()",
					"{",
						"SceneManager.sceneLoaded += (o,n) =>",
						"{",
							"foreach (var system in systems)",
							"{",
								"system.Reset();",
							"}",
							"DetectAllEntities();",
						"};",
						"SceneManager.sceneUnloaded += (o) =>",
						"{",
							"foreach (var system in systems)",
							"{",
								"system.Reset();",
							"}",
							"DetectAllEntities();",
						"};",
					"}",
					"",
					"void Update()",
					"{",
						"foreach (var system in systems)",
						"{",
							"system.Run();",
						"}",
					"}",
					"",
					"public static GameObject Create(GameObject prefab)",
					"{",
						"var go = GameObject.Instantiate<GameObject>(prefab);",
						"CreateRecursive(go);",
						"return go;",
					"}",
					"",
					"public static void CreateRecursive(GameObject go)",
					"{",
						"foreach (var system in systems)",
						"{",
							"system.Classify(go);",
						"}",
						"foreach (Transform child in go.GetComponent<Transform>())",
						"{",
							"CreateRecursive(child.gameObject);",
						"}",
					"}",
					"",
					"public static void Destroy(GameObject go)",
					"{",
						"DestroyRecursive(go);",
						"GameObject.Destroy(go);",
					"}",
					"",
					"public static void DestroyRecursive(GameObject go)",
					"{",
						"foreach (var system in systems)",
						"{",
							"system.Destroy(go);",
						"}",
						"foreach (Transform child in go.GetComponent<Transform>())",
						"{",
							"DestroyRecursive(child.gameObject);",
						"}",
					"}",
				"}",
			"}"
		);
	}

	private String generateSystem(UserFunction system)
	{
		namespaces.clear();
		namespaces.add("UnityEngine");
		namespaces.add("System.Collections.Generic");

		currentFunction = system;
		overlapNames = new HashMap<>();

		var components = extraComponents(system);
		for (var role : currentFunction.roles.entrySet())
		{
			if (!components.containsKey(role.getKey()))
			{
				components.put(role.getKey(), new HashSet<>());
			}
			for (var component : role.getValue().keySet())
			{
				components.get(role.getKey()).add(component);
			}
		}

		var lines = lines
		(
			"namespace M",
			"{",
				"public class "+unreserved(system.getName())+" : ISystem",
				"{",
					foreach(system.roles.keySet(), x -> lines
					(
						"struct "+x,
						"{",
							"public GameObject _go;",
							foreach(components.get(x), c->"public "+component(c)+" "+c+";"),
						"}"
					)),
					foreach(system.roles.keySet(), x->"List<"+x+"> "+x+"_list = new List<"+x+">();"),
					"",
					"public void Reset()",
					"{",
						foreach(system.roles.keySet(), x -> lines
						(
							x+"_list.Clear();"
						)),
					"}",
					"",
					"public void Destroy(GameObject go)",
					"{",
						foreach(system.roles.keySet(), x->lines
						(
							"for (var i = "+x+"_list.Count - 1; i >= 0; i--)",
							"{",
								"if ("+x+"_list[i]._go == go)",
								"{",
									x+"_list.RemoveAt(i);",
								"}",
							"}"
						)),
					"}",
					"public void Classify(GameObject go)",
					"{",
						foreach(system.roles.keySet(), e->lines
						(
							iff(!components.get(e).isEmpty()),
							"{",
								"if ("+foreach(components.get(e), c-> "go.GetComponent<"+component(c)+">() is var "+c+" && "+c, " && ")+")",
								"{",
								end,
									e+"_list.Add(new "+e+"{ _go = go, "+foreach(components.get(e), c-> c+" = "+c, ", ")+" });",
								iff(!components.get(e).isEmpty()),
								"}",
							"}",
							end
						)),
					"}",
					"",
					"public void Run()",
					"{",
						foreach(system.getStatements(), s->code(s)),
					"}",
				"}",
			"}"
		);

		return write
		(
			foreach(namespaces, n->"using "+n+";"),
			"",
			lines
		);
	}

	private ArrayList<Application> overlaps(EObject o)
	{
		var list = new ArrayList<Application>();
		for (var application : EcoreUtil2.getAllContentsOfType(o, Application.class))
		{
			if (library.getFunction(application.getName()) == OVERLAPS)
			{
				list.add(application);
			}
		}
		return list;
	}

	private Object code(Statement statement)
	{
		Object result = "undefined";

		if (statement instanceof BindingBlock)
		{
			stack.push(new HashMap<>(variables));

			var block = (BindingBlock) statement;
			var name = block.getName();
			if (library.query.equals(name))
			{
				var a = block.getExpression().getName();
				List<Object> lines = null;

				var binaries = EcoreUtil2.getAllContentsOfType(block, Binary.class);
				for (var binary : binaries)
				{
					if (binary.getOperator().equals(library.getName(m.library.symbols.Function.HIT_IN)) && ((Value)binary.getLeft()).getName().equals(a))
					{
						lines = lines
						(
						"for (var i_"+a+" = "+code(binary.getRight())+".Count - 1; i_"+a+" >= 0; i_"+a+"--)",
						"{",
							"var "+a+" = "+code(binary.getRight())+"[i_"+a+"];",
							foreach(block.getStatements(), s->code(s)),
						"}"
						);

						variables = stack.pop();

						result = lines;
					}
				}

				if (lines == null)
				{
					variables.put(a,true);

					lines = lines
					(
					"for (var i_"+a+" = "+a+"_list.Count - 1; i_"+a+" >= 0; i_"+a+"--)",
					"{",
						"var "+a+" = "+a+"_list[i_"+a+"];",
						foreach(block.getStatements(), s->code(s)),
					"}"
					);

					variables = stack.pop();

					result = lines;
				}
			}
			else
			{
				result = "undefined";
			}
		}
		else if (statement instanceof Block)
		{
			stack.push(new HashMap<>(variables));
			var block = (Block) statement;
			var name = block.getName();

			if (library.selection.equals(name))
			{
				var overlaps = overlaps(block.getExpression());
				for (var overlap : overlaps)
				{
					overlapNames.put(overlap, "collisions_"+new Random().nextInt(1000000));
				}

				var expression = block.getExpression();
				if (expression instanceof Binary && ((Binary)expression).getOperator().equals(library.getName(m.library.symbols.Function.HIT_IN)))
				{
					result = foreach(block.getStatements(), s->code(s));
				}
				else
				{
					var condition = code(block.getExpression());

					result = lines
					(
						"if ("+condition+")",
						"{",
							foreach(block.getStatements(), s->code(s)),
						"}"
					);
				}
			}
			else if (library.iteration.equals(name))
			{
				var overlaps = overlaps(block.getExpression());
				for (var overlap : overlaps)
				{
					overlapNames.put(overlap, "collisions_"+new Random().nextInt(1000000));
				}

				var condition = code(block.getExpression());

				for (var overlap : overlaps)
				{
					overlapNames.put(overlap, "collisions_"+new Random().nextInt(1000000));
				}
				result = lines
				(
					"while ("+condition+")",
					"{",
						foreach(block.getStatements(), s->code(s)),
					"}"
				);
			}
			variables = stack.pop();
		}
		else if (statement instanceof Assignment)
		{
			var assignment = (Assignment) statement;
			var atom = assignment.getAtom();
			var expression = assignment.getExpression();
			var code = code(expression);

			var overlaps = overlaps(expression);
			for (var overlap : overlaps)
			{
				overlapNames.put(overlap, "collisions_"+new Random().nextInt(1000000));
			}

			if (atom instanceof Value)
			{
				var value = (Value) atom;
				var name = value.getName();
				if (variables.keySet().contains(name))
				{
					result = code(atom)+" = "+code+";";
				}
				else
				{
					variables.put(name,false);
					result = "var "+code(atom)+" = "+code+";";
				}
			}
			else if (atom instanceof Cell)
			{
				var cell = (Cell) atom;
				var entity = cell.getEntity().getName();
				var component = cell.getComponent().getName();

				if (library.getComponent(cell.getComponent().getName()) == DISPLAY)
				{
					code = "(int)("+code+")";
				}

				if (currentFunction.roles.containsKey(entity))
				{
					result = code(atom)+" = "+code+";";
				}
				else
				{
					result = entity+".GetComponent<"+component(component)+">()."+field(component)+" = "+code+";";
				}
			}
		}
		else if (statement instanceof Delegation)
		{
			var delegation = (Delegation) statement;
			var application = delegation.getApplication();

			var overlaps = overlaps(application);
			for (var overlap : overlaps)
			{
				overlapNames.put(overlap, "collisions_"+new Random().nextInt(1000000));
			}

			result = code(application)+";";
		}

		return result;
	}

	private String code(Expression e)
	{
		if (e instanceof Binary)
		{
			var binary = (Binary) e;
			var list = new ArrayList<Expression>();
			list.add(binary.getLeft());
			list.add(binary.getRight());
			return application(library.getFunction(binary.getOperator()), list);
		}
		else if (e instanceof Unary)
		{
			var unary = (Unary) e;
			var list = new ArrayList<Expression>();
			list.add(unary.getExpression());
			return application(library.getFunction(unary.getOperator()), list);
		}
		else if (e instanceof Value)
		{
			var value = (Value) e;
			return access(value.getName());
		}
		else if (e instanceof Cell)
		{
			var cell = (Cell) e;
			var component = cell.getComponent().getName();
			var entity = cell.getEntity().getName();

			return entity+"."+component+"."+field(component);
		}
		else if (e instanceof Application)
		{
			var application = (Application) e;
			var name = application.getName();
			var args = application.getArguments();
			var standard = library.getFunction(name);

			return application(standard, args);
		}
		return "undefined";
	}

	private String application(m.library.symbols.Function standard, List<Expression> args)
	{
		var x = "";
		var y = "";
		var z = "";
		var w = "";

		if (!args.isEmpty())
		{
			x = code(args.get(0));
		}
		if (args.size() >= 2)
		{
			y = code(args.get(1));
		}
		if (args.size() >= 3)
		{
			z = code(args.get(2));
		}
		if (args.size() >= 4)
		{
			w = code(args.get(3));
		}

		// TODO color functions
		switch (standard)
		{
		case ABS: namespaces.add("Unity.Mathematics"); return "math.abs("+x+")";
		case ACOS: namespaces.add("Unity.Mathematics"); return "math.acos("+x+")";
		case ADDITION: return "("+x+" + "+y+")";
		case AND: return "("+x+" && "+y+")";
		case ASIN: namespaces.add("Unity.Mathematics"); return "math.asin("+x+")";
		case ASSIGNMENT: return x+" = "+y;
		case ATAN: namespaces.add("Unity.Mathematics"); return "math.atan("+x+")";
		case CEIL: namespaces.add("Unity.Mathematics"); return "math.ceil("+x+")";
		case CLAMP: namespaces.add("Unity.Mathematics"); return "math.clamp("+x+", "+y+".x, "+y+".y)";
		case COS: namespaces.add("Unity.Mathematics"); return "math.cos("+x+")";
		case CREATE: return "SystemRunner.Create("+x+")";
		case CROSS: namespaces.add("Unity.Mathematics"); return "Vector3.Cross("+x+","+y+")";
		case DESTROY: return "SystemRunner.Destroy("+x+")";
		case DISTANCE: namespaces.add("Unity.Mathematics"); return "math.distance("+x+", "+y+")";
		case DIVISION: return "("+x+" / "+y+")";
		case DOT: namespaces.add("Unity.Mathematics"); return "math.dot("+x+", "+y+")";
		case EQUAL: return "("+x+" == "+y+")";
		case EXP: namespaces.add("Unity.Mathematics"); return "math.exp("+x+")";
		case FLOOR: namespaces.add("Unity.Mathematics"); return "math.floor("+x+")";
		case FRACTIONAL_PART: namespaces.add("Unity.Mathematics"); return "math.frac("+x+")";
		case GREATER: return "("+x+" > "+y+")";
		case GREATER_OR_EQUAL: return "("+x+" >= "+y+")";
		case HALT: return "#if UNITY_EDITOR\nUnityEditor.EditorApplication.isPlaying = false;\n#endif\nApplication.Quit()";
		case IN: return y+".Contains("+x+")";
		case UNEQUAL: return "("+x+" != "+y+")";
		case INTEGER_PART: namespaces.add("Unity.Mathematics"); return "math.trunc("+x+")";
		case INVERSE: return "(1 / "+x+")";
		case LINEAR_INTERPOLATION: namespaces.add("Unity.Mathematics"); return "math.lerp("+x+", "+y+".x, "+y+".y)";
		case LOG: namespaces.add("Unity.Mathematics"); return "math.log("+x+")";
		case LOWER: return "("+x+" < "+y+")";
		case LOWER_OR_EQUAL: return "("+x+" <= "+y+")";
		case MULTIPLICATION: return "("+x+" * "+y+")";
		case QUATERNION_MULTIPLICATION: return x+" * "+y;
		case NORM: namespaces.add("Unity.Mathematics"); return "math.length("+x+")";
		case NORMALIZE: namespaces.add("Unity.Mathematics"); return x+".normalized";
		case NOT: return "! "+x;
		case OR: return "("+x+" || "+y+")";
		case PLAY_ONCE: return x.replace("._go", "")+".AudioSource.PlayOneShot("+y+")";
		case POW: namespaces.add("Unity.Mathematics"); return "math.pow("+x+", "+y+")";
		case PROPORTIONAL: namespaces.add("Unity.Mathematics"); return "math.remap("+x+", "+y+".x, "+y+".y, "+z+".x, "+z+".y)";
		case RANDOM: return "UnityEngine.Random.Range("+x+".x, "+x+".y)";
		case READ_NUMBER: return x+".ReadValue<float>()";
		case READ_TRIGGERED: return x+".triggered";
		case READ_VECTOR: return x+".ReadValue<Vector2>()";
		case RECIPROCAL: return "-"+x;
		case REFLECT: namespaces.add("Unity.Mathematics"); return "Vector3.Reflect("+x+", "+y+")";
		case REFRACT: namespaces.add("Unity.Mathematics"); return "((Vector3)math.refract("+x+", "+y+", "+z+"))";
		case ROUND: namespaces.add("Unity.Mathematics"); return "math.round("+x+")";
		case SET_COLOR: return x+".SetColor("+y+", "+z+")";
		case SET_NUMBER: return x+".SetFloat("+y+", "+z+")";
		case ACTIVATE_ANIMATOR_TRIGGER: return x.replace("._go", "")+".Animator.SetTrigger("+y+")";
		case SIGN: namespaces.add("Unity.Mathematics"); return "math.sign("+x+")";
		case SIN: namespaces.add("Unity.Mathematics"); return "math.sin("+x+")";
		case SIZE: return x+".Count";
		case SQRT: namespaces.add("Unity.Mathematics"); return "math.sqrt("+x+")";
		case IN_STATE: return x.replace("._go", "")+".Animator.GetCurrentAnimatorStateInfo(0).IsName("+y+")";
		case SUBTRACTION: return "("+x+" - "+y+")";
		case TAN: namespaces.add("Unity.Mathematics"); return "math.tan("+x+")";
		case INVERSE_LINEAR_INTERPOLATION: namespaces.add("Unity.Mathematics"); return "math.unlerp("+x+", "+y+".x, "+y+".y)";
		case WRITE: return "if (Debug.isDebugBuild){ Debug.Log("+x+"); }";
		case WRITE_ERROR: return "if (Debug.isDebugBuild){ Debug.LogError("+x+"); }";
		case WRITE_WARNING: return "if (Debug.isDebugBuild){ Debug.LogWarning("+x+"); }";
		case SCREENSHOT: return "ScreenCapture.CaptureScreenshot((Time.time+\".png\").Replace(\"/\", \"-\"), 1)";
		case XY: return "new Vector2("+x+", "+y+")";
		case XYZ: return "new Vector3("+x+", "+y+", "+z+")";
		case OVERLAPS:
			namespaces.add("System.Linq");
			namespaces.add("System");
			return x+".GetComponents<Collider>().Select(x=> x is BoxCollider ? Physics.OverlapBox((x as BoxCollider).bounds.center, Vector3.Scale((x as BoxCollider).size/2,"+x+".transform.lossyScale), "+x+".transform.rotation, Int32.MaxValue, QueryTriggerInteraction.Collide): x is SphereCollider ? Physics.OverlapSphere((x as SphereCollider).bounds.center, (x as SphereCollider).radius*Mathf.Max("+x+".transform.lossyScale.x, Mathf.Max("+x+".transform.lossyScale.y, "+x+".transform.lossyScale.z)), Int32.MaxValue, QueryTriggerInteraction.Collide) : null).Aggregate(new List<Collider>(), (list, x) => {list.AddRange(x); return list;}).Select(x=>x.transform.gameObject).ToList()";
		case TO_RECTANGLE: return "new Rect("+x+", "+y+", "+z+", "+w+")";
		case TO_NUMBER3: return x+".eulerAngles";
		case TO_QUATERNION: return "Quaternion.Euler("+x+".x, "+x+".y, "+x+".z)";
		case ADD_FORCE: return x.replace("._go", "")+".Rigidbody.AddForce("+y+")";
		case ADD_TORQUE: return x.replace("._go", "")+".Rigidbody.AddTorque("+y+")";
		case CLOSEST_POINT: return "("+x+".GetComponent<Collider>() != null ? "+x+".GetComponent<Collider>().ClosestPoint("+y+")" + ":" + x+".transform.position)";
		case ANGLE_BETWEEN: namespaces.add("Unity.Mathematics"); return "(math.atan2("+x+".y, "+x+".x)-math.atan2("+y+".y, "+y+".x))";
		case QUATERNION_BETWEEN: return "Quaternion.FromToRotation("+x+", "+y+")";
		case GET_COLOR: return x+".GetColor("+y+")";
		case GET_INTEGER: return x+".GetInt("+y+")";
		case GET_KEYWORD: return x+".IsKeywordEnabled("+y+")";
		case GET_NUMBER: return x+".GetFloat("+y+")";
		case GET_TEXTURE: return x+".GetTexture("+y+")";
		case SET_INTEGER: return x+".SetInt("+y+", (int)("+z+"))";
		case SET_KEYWORD: return "if ("+z+"){ "+x+".EnableKeyword("+y+"); }else{ "+x+".DisableKeyword("+y+"); }";
		case SET_TEXTURE: return x+".SetTexture("+y+", "+z+")";
		case DEGREES: namespaces.add("Unity.Mathematics"); return "math.degrees("+x+")";
		case MAX: namespaces.add("Unity.Mathematics"); return "math.max("+x+", "+y+")";
		case MIN: namespaces.add("Unity.Mathematics"); return "math.min("+x+", "+y+")";
		case RADIANS: namespaces.add("Unity.Mathematics"); return "math.radians("+x+")";
		case SPHERICAL_INTERPOLATION: namespaces.add("Unity.Mathematics"); return "math.slerp("+x+", "+y+", "+z+")";
		case STEP: namespaces.add("Unity.Mathematics"); return "math.step("+x+", "+y+")";
		case BREAKPOINT: return "Debug.Break()";
		case PAUSE: return x.replace("._go", "")+".AudioSource.Pause()";
		case PLAY: return x.replace("._go", "")+".AudioSource.Play()";
		case STOP: return x.replace("._go", "")+".AudioSource.Stop()";
		case START_RECORDING: return "Microphone.Start("+x+", false, (int)"+y+", 44100)";
		case STOP_RECORDING: return "Microphone.End("+x+")";
		case VIEWPORT_TO_WORLD: return "("+y+").GetComponent<Camera>().ViewportToWorldPoint("+x+")";
		case WORLD_TO_VIEWPORT: return "("+y+").GetComponent<Camera>().WorldToViewportPoint("+x+")";
		case RAY: return "new List<RaycastHit>(Physics.RaycastAll("+x+", "+y+", "+z+"))";
		case BOX: return "new List<RaycastHit>(Physics.BoxCastAll("+x+", "+y+", Vector3.zero, "+z+", 0))";
		case SPHERE: return "new List<RaycastHit>(Physics.SphereCastAll("+x+", "+y+", Vector3.zero, 0))";
		case SPATIAL_X: return x+".x";
		case SPATIAL_Y: return x+".y";
		case SPATIAL_Z: return x+".z";
		case TO_NUMBER: return "float.Parse("+x+")";
		case TO_TEXT: return x+".ToString()";
		case IS_NEGATIVE: return "("+x+" < 0)";
		case IS_POSITIVE: return "("+x+" > 0)";
		case IS_ZERO: return "("+x+" == 0)";
		case ACTIVATE_PARAMETER: return x.replace("._go", "")+".Animator.SetBool("+y+", true)";
		case DEACTIVATE_PARAMETER: return x.replace("._go", "")+".Animator.SetBool("+y+", false)";
		case PLAY_ANIMATION: return x.replace("._go", "")+".Animator.Play("+y+")";
		case EVALUATE: return x+".Evaluate("+y+")";
		case ABSCISSA: return x+".x";
		case ALPHA: return x+".a";
		case BLUE: return x+".b";
		case DRAW_RAY: return "Debug.DrawRay("+x+", "+y+")";
		case GREEN: return x+".g";
		case HEIGHT: return x+".height";
		case HIT_IN: return y+".Contains("+x+")";
		case HIT_ENTITY: return x+".transform.gameObject";
		case HIT_NORMAL: return x+".normal";
		case HIT_POINT: return x+".point";
		case ORDINATE: return x+".y";
		case PLANAR_ADDITION: return "("+x+" + "+y+")";
		case PLANAR_DISTANCE: return "Vector2.Distance("+x+", "+y+")";
		case PLANAR_DIVISION: return "("+x+" / "+y+")";
		case PLANAR_MULTIPLICATION: return "("+x+" * "+y+")";
		case PLANAR_NORM: namespaces.add("Unity.Mathematics"); return "math.length("+x+")";
		case PLANAR_NORMALIZE: namespaces.add("Unity.Mathematics"); return x+".normalized";
		case PLANAR_SUBTRACTION: return "("+x+" - "+y+")";
		case PLANAR_X: return x+".x";
		case PLANAR_Y: return x+".y";
		case RED: return x+".r";
		case REMAINDER: return "("+x+" % "+y+")";
		case RGBA: return "new Color("+x+", "+y+", "+z+", "+w+")";
		case SET_TIME_SPEED: return "Time.timeScale = "+x+";"+"Time.fixedDeltaTime = "+x+"/50";
		case SPATIAL_ADDITION: return "("+x+" + "+y+")";
		case SPATIAL_DIVISION: return "("+x+" / "+y+")";
		case SPATIAL_MULTIPLICATION: return "("+x+" * "+y+")";
		case SPATIAL_SUBTRACTION: return "("+x+" - "+y+")";
		case WIDTH: return x+".width";
		case TICK: return x+" = "+x+" - Time.deltaTime";
		case WORLD_POSITION: return x+".transform.position";
		case WORLD_ROTATION: return x+".transform.rotation";
		case IS_PARENT: return "("+y+".transform.parent == "+x+")";
		case SET_PARENT: return y+".transform.SetParent("+x+".transform)";
		case CLEAR_PARENT: return x+".transform.SetParent(null)";
		case SET_WORLD_POSITION: return x+".transform.position = "+y;
		case ENABLE: return x+".SetActive(true); SystemRunner.CreateRecursive("+x+")";
		case DISABLE: return x+".SetActive(false); SystemRunner.DestroyRecursive("+x+")";
		case BITWISE_AND: return "((int)"+x+" & (int)"+y+")";
		case BITWISE_OR: return "((int)"+x+" | (int)"+y+")";
		case BITWISE_XOR: return "((int)"+x+" ^ (int)"+y+")";
		case BITWISE_NOT: return "(~(int)"+x+")";
		case SPATIAL_ANGLE: return "Vector3.Angle("+x+", "+y+")";
		}
		return "undefined";
	}

	private HashMap<String, HashSet<String>> extraComponents(Function function)
	{
		var map = new HashMap<String, HashSet<String>>();

		for (var application : EcoreUtil2.getAllContentsOfType(function, Application.class))
		{
			var name = application.getName();
			var standard = game.library.getFunction(name);
			if (standard == ACTIVATE_ANIMATOR_TRIGGER || standard == IN_STATE || standard == ACTIVATE_PARAMETER || standard == DEACTIVATE_PARAMETER || standard == PLAY_ANIMATION )
			{
				if (application.getArguments().get(0) instanceof Value)
				{
					var entity = ((Value)application.getArguments().get(0)).getName();
					if (!map.containsKey(entity))
					{
						map.put(entity, new HashSet<>());
					}
					map.get(entity).add("Animator");
				}
			}
			else if (standard == PLAY || standard == PLAY_ONCE || standard == PAUSE || standard == STOP)
			{
				if (application.getArguments().get(0) instanceof Value)
				{
					var a = ((Value)application.getArguments().get(0)).getName();
					if (!map.containsKey(a))
					{
						map.put(a, new HashSet<>());
					}
					map.get(a).add("AudioSource");
				}
			}
			else if (standard == ADD_FORCE || standard == ADD_TORQUE)
			{

				if (application.getArguments().get(0) instanceof Value)
				{
					var a = ((Value)application.getArguments().get(0)).getName();
					if (!map.containsKey(a))
					{
						map.put(a, new HashSet<>());
					}
					map.get(a).add("Rigidbody");
				}
			}
			else if (standard == VIEWPORT_TO_WORLD || standard == WORLD_TO_VIEWPORT)
			{
				if (application.getArguments().get(1) instanceof Value)
				{
					var a = ((Value)application.getArguments().get(1)).getName();
					if (!map.containsKey(a))
					{
						map.put(a, new HashSet<>());
					}
					map.get(a).add("Camera");
				}
			}
		}
		return map;
	}


	private String variable(String name)
	{
		var found = library.getValue(name);
		if (found != null)
		{
			switch (found)
			{
			case TRUE:
				return "true";
			case FALSE:
				return "false";
			case EPSILON:
				namespaces.add("Unity.Mathematics");
				return "math.FLT_MIN_NORMAL";
			case PI:
				namespaces.add("Unity.Mathematics");
				return "math.PI";
			case E:
				namespaces.add("Unity.Mathematics");
				return "math.E";
			case INFINITY:
				namespaces.add("Unity.Mathematics");
				return "math.INFINITY";
			case UP:
				return "Vector3.up";
			case RIGHT:
				return "Vector3.right";
			case FORWARD:
				return "Vector3.forward";
			case SCREEN_WIDTH:
				return "Screen.width";
			case SCREEN_HEIGHT:
				return "Screen.height";
			case TIME_SINCE_START:
				namespaces.add("UnityEngine");
				return "UnityEngine.Time.time";
			case DELTA_TIME:
				namespaces.add("UnityEngine");
				return "UnityEngine.Time.deltaTime";
			case TIME_SPEED:
				namespaces.add("UnityEngine");
				return "UnityEngine.Time.timeScale";
			case ONE:
				return "1f";
			case ZERO:
				return "0f";
			case DEFAULT_AUDIO_INPUT:
				return "null";
			case HDMI_AUDIO_INPUT:
				namespaces.add("System.Linq");
				return "Microphone.devices.Where(x => x.Contains(\"HDMI\")).First()";
			case USB_AUDIO_INPUT:
				namespaces.add("System.Linq");
				return "Microphone.devices.Where(x => x.Contains(\"USB\")).First()";
			}
			return "undefinedVariable";
		}
		else if (variables.containsKey(name) && variables.get(name))
		{
			return name+"._go";
		}
		else
		{
			return name;
		}
	}

	private String access(String name)
	{
		return variable(name);
	}

	private String unreserved(String name)
	{
		for (var i = 0; i < csharpReserved.length; i++)
		{
			if (csharpReserved[i].equals(name))
			{
				return "_"+name;
			}
		}
		return name;
	}

	private String component(String name)
	{
		var found = library.getComponent(name);
		if (name.equals("Animator"))
		{
			return "Animator";
		}
		else if (name.equals("AudioSource"))
		{
			return "AudioSource";
		}
		else if (name.equals("Rigidbody"))
		{
			return "Rigidbody";
		}
		else if (name.equals("Camera"))
		{
			return "Camera";
		}

		if (found == null)
		{
			for (var i = 0; i < csharpReserved.length; i++)
			{
				if (csharpReserved[i].equals(name))
				{
					return "_"+name;
				}
			}
			return "M."+name;
		}
		else
		{
			switch (found)
			{
				case POSITION:
				case ROTATION:
				case SCALE: return "Transform";

				case KINEMATIC:
				case MASS:
				case ANGULAR_MASS:
				case VELOCITY:
				case ANGULAR_VELOCITY: return "Rigidbody";

				case BOX_CENTER:
				case EXTENTS: return "BoxCollider";

				case SPHERE_CENTER:
				case RADIUS: return "SphereCollider";

				case MESH: return "MeshFilter";
				case MATERIAL: return "Renderer";

				case NEAR_PLANE:
				case FAR_PLANE:
				case FIELD_OF_VIEW:
				case PERSPECTIVE:
				case DISPLAY:
				case VIEWPORT:
				case BACKGROUND: return "Camera";

				case EMISSION:
				case COOKIE:
				case COOKIE_SIZE:
				case INTENSITY:
				case BOUNCE_INTENSITY:
				case RANGE:
				case SPOT_ANGLE: return "Light";

				case AUDIO_CLIP:
				case VOLUME:
				case PITCH:
				case LOOP:
				case MUTE: return "AudioSource";

				case TEXT:
				case TEXT_MATERIAL:
				case FONT: namespaces.add("UnityEngine.UI"); return "Text";

				case IMAGE:
				case IMAGE_MATERIAL: namespaces.add("UnityEngine.UI"); return "RawImage";

				case DESTINATION:
				case MAX_ACCELERATION:
				case MAX_ANGULAR_SPEED:
				case MAX_SPEED:
				case TRAVERSABLE_AREAS: namespaces.add("UnityEngine.AI"); return "NavMeshAgent";

				case ANIMATOR_CONTROLLER: return "Animator";

				case SCREEN_POSITION: return "RectTransform";
		}
		}
		return "undefined";
	}

	private String field(String name)
	{
		var found = library.getComponent(name);
		if (found == null)
		{
			return "Value";
		}
		else
		{
			switch (found)
			{
			case VELOCITY: return "velocity";
			case POSITION: return "localPosition";
			case ANGULAR_VELOCITY: return "angularVelocity";
			case AUDIO_CLIP: return "clip";
			case BACKGROUND: return "backgroundColor";
			case EMISSION: return "color";
			case EXTENTS: return "size";
			case FAR_PLANE: return "farClipPlane";
			case FIELD_OF_VIEW: return "fieldOfView";
			case ANGULAR_MASS: return "inertiaTensor";
			case INTENSITY: return "intensity";
			case LOOP: return "loop";
			case MASS: return "mass";
			case MATERIAL: return "material";
			case MESH: return "sharedMesh";
			case NEAR_PLANE: return "nearClipPlane";
			case PITCH: return "pitch";
			case RADIUS: return "radius";
			case RANGE: return "range";
			case ROTATION: return "localRotation";
			case SCALE: return "localScale";
			case SPOT_ANGLE: return "spotAngle";
			case VOLUME: return "volume";
			case BOX_CENTER: return "center";
			case SPHERE_CENTER: return "center";
			case KINEMATIC: return "isKinematic";
			case BOUNCE_INTENSITY: return "bounceIntensity";
			case COOKIE: return "cookie";
			case DISPLAY: return "targetDisplay";
			case FONT: return "font";
			case IMAGE: return "texture";
			case IMAGE_MATERIAL: return "material";
			case TEXT: return "text";
			case TEXT_MATERIAL: return "material";
			case DESTINATION: return "destination";
			case MAX_ACCELERATION: return "acceleration";
			case MAX_ANGULAR_SPEED: return "angularSpeed";
			case MAX_SPEED: return "speed";
			case TRAVERSABLE_AREAS: return "areaMask";
			case ANIMATOR_CONTROLLER: return "runtimeAnimatorController";
			case COOKIE_SIZE: return "cookieSize";
			case MUTE: return "mute";
			case PERSPECTIVE: return "orthographic";
			case SCREEN_POSITION: return "undefined";
			case VIEWPORT: return "rect";
			}
		}
		return "undefined";
	}

	private String code(Type type)
	{
		if (type instanceof AtomicType)
		{
			var atomic = (AtomicType) type;
			switch (atomic)
			{
			case ANYTHING: return "object";
			case ENTITY:
					namespaces.add("UnityEngine");
					return "GameObject";
			case NUMBER:
				namespaces.add("System");
				return "Single";
			case NUMBER2:
					namespaces.add("UnityEngine");
					return "Vector2";
			case NUMBER3:
				namespaces.add("UnityEngine");
				return "Vector3";
			case PROPOSITION:
				return "bool";
			case ENTITY_LIST:
				namespaces.add("System.Collections.Generic");
				namespaces.add("UnityEngine");
				return "List<GameObject>";
			case INPUT:
				namespaces.add("UnityEngine.InputSystem");
				return "InputAction";
			case TEXT:
				return "string";
			case NOTHING:
				return "void";
			case COLOR:
				namespaces.add("UnityEngine");
				return "Color";
			case MESH:
				namespaces.add("UnityEngine");
				return "Mesh";
			case MATERIAL:
				namespaces.add("UnityEngine");
				return "Material";
			case ANIMATOR:
				namespaces.add("UnityEngine");
				return "RuntimeAnimatorController";
			case CURVE:
				namespaces.add("UnityEngine");
				return "AnimationCurve";
			case FONT:
				namespaces.add("UnityEngine.UI");
				return "Font";
			case AUDIO:
				namespaces.add("UnityEngine");
				return "AudioClip";
			case QUATERNION:
				namespaces.add("UnityEngine");
				return "Quaternion";
			case IMAGE:
				namespaces.add("UnityEngine");
				return "Texture";
			case RECTANGLE:
				namespaces.add("UnityEngine");
				return "Rect";
			case HIT:
				return "RaycastHit";
			case HIT_LIST:
				namespaces.add("System.Collections.Generic");
				namespaces.add("UnityEngine");
				return "List<RaycastHit>";
			}
			return "Undefined";
		}
		return "Undefined";
	}

	private void clean(String root)
	{
		cleanCSharpFiles(Paths.get(root,"Components").toString());
		cleanCSharpFiles(Paths.get(root,"Systems").toString());
		cleanCSharpFiles(Paths.get(root,"Main").toString());
		cleanCSharpFiles(Paths.get(root,"Editor").toString());
	}

	private void cleanCSharpFiles(String path)
	{
		var directory = new File(path);
		if (!directory.exists())
		{
			return;
		}
		for (var file : directory.listFiles())
		{
			if (file.getName().endsWith(".cs"))
			{
				file.delete();
			}
			if (file.getName().endsWith((".cs.meta")))
			{
				file.delete();
			}
		}
	}
}

class AssemblyDefinition
{
	String name;
	List<String> references;
	List<String> includePlatforms;
	List<String> excludePlatforms;
	boolean allowUnsafeCode;
	boolean overrideReferences;
	List<String> precompiledReferences;
	boolean autoReferenced;
	List<String> definedConstraints;
	List<String> versionDefines;
	boolean noEngineReferences;

	public AssemblyDefinition(String name, List<String> references, boolean allowUnsafeCode)
	{
		this.name = name;
		this.references = references;
		this.allowUnsafeCode = allowUnsafeCode;
	}
}

class PackageManifest
{
	Map<String,String> dependencies;

	public PackageManifest(Map<String,String> dependencies)
	{
		this.dependencies = dependencies;
	}

	public Map<String,String> getDependencies()
	{
		return dependencies;
	}
}
