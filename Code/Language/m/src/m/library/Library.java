package m.library;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import m.library.rules.BindingReason;
import m.library.rules.Classification;
import m.library.rules.ProblemKind;
import m.library.rules.TypingReason;
import m.library.symbols.Component;
import m.library.symbols.Function;
import m.library.symbols.Value;
import m.library.symbols.Symbol;
import m.library.types.AtomicType;
import m.library.types.FunctionType;
import m.library.types.Type;
import m.library.types.TypeVariable;

public enum Library
{
	ENGLISH
	(
		"foreach",
		"if",
		"while",

	value -> {switch(value)
	{
		case TRUE: return "true";
		case FALSE: return "false";
		case EPSILON: return "epsilon";
		case E: return "e";
		case PI: return "pi";
		case INFINITY: return "infinity";
		case UP: return "up";
		case RIGHT: return "right";
		case FORWARD: return "forward";
		case SCREEN_WIDTH: return "screenWidth";
		case SCREEN_HEIGHT: return "screenHeight";
		case TIME_SINCE_START: return "timeSinceStart";
		case DELTA_TIME: return "deltaTime";
		case TIME_SPEED: return "timeSpeed";
		case ONE: return "one";
		case ZERO: return "zero";
		case DEFAULT_AUDIO_INPUT: return "defaultAudioInput";
		case HDMI_AUDIO_INPUT: return "hdmiAudioInput";
		case USB_AUDIO_INPUT: return "usbAudioInput";
	} return "";},
	component -> {switch(component)
	{
		case POSITION: return "position";
		case ROTATION: return "rotation";
		case SCALE: return "scale";

		case MASS: return "mass";
		case ANGULAR_MASS: return "angularMass";

		case VELOCITY: return "velocity";
		case ANGULAR_VELOCITY: return "angularVelocity";
		case KINEMATIC: return "kinematic";

		case RADIUS: return "radius";
		case EXTENTS: return "extents";
		case BOX_CENTER: return "boxCenter";
		case SPHERE_CENTER: return "sphereCenter";

		case MESH: return "mesh";
		case MATERIAL: return "material";

		case NEAR_PLANE: return "nearPlane";
		case FAR_PLANE: return "farPlane";
		case FIELD_OF_VIEW: return "fieldOfView";
		case BACKGROUND: return "background";
		case DISPLAY: return "display";

		case EMISSION: return "emission";
		case SPOT_ANGLE: return "spotAngle";
		case RANGE: return "range";
		case INTENSITY: return "intensity";
		case BOUNCE_INTENSITY: return "bounceIntensity";
		case COOKIE: return "cookie";

		case TEXT: return "text";
		case FONT: return "font";
		case TEXT_MATERIAL: return "textMaterial";

		case IMAGE: return "image";
		case IMAGE_MATERIAL: return "imageMaterial";

		case AUDIO_CLIP: return "audioClip";
		case VOLUME: return "volume";
		case PITCH: return "pitch";
		case LOOP: return "loop";

		case DESTINATION: return "destination";
		case MAX_ACCELERATION: return "maxAcceleration";
		case MAX_ANGULAR_SPEED: return "maxAngularSpeed";
		case TRAVERSABLE_AREAS: return "traversableAreas";
		case MAX_SPEED: return "maxSpeed";
		case ANIMATOR_CONTROLLER: return "animatorController";
		case COOKIE_SIZE: return "cookieSize";
		case MUTE: return "mute";
		case PERSPECTIVE: return "perspective";
		case SCREEN_POSITION: return "screenPosition";
		case VIEWPORT: return "viewport";
	} return "";},
	function -> {switch(function)
	{
		case ABS: return "abs";
		case SIGN: return "sign";
		case CEIL: return "ceil";
		case FLOOR: return "floor";
		case ROUND: return "round";
		case INTEGER_PART: return "integerPart";
		case FRACTIONAL_PART: return "fractionalPart";
		case INVERSE: return "inverse";
		case RECIPROCAL: return "reciprocal";

		case CLAMP: return "clamp";
		case LINEAR_INTERPOLATION: return "linearInterpolation";
		case INVERSE_LINEAR_INTERPOLATION: return "inverseLinearInterpolation";
		case PROPORTIONAL: return "proportional";
		case SPHERICAL_INTERPOLATION: return "sphericalInterpolation";

		case MIN: return "min";
		case MAX: return "max";

		case DEGREES: return "degrees";
		case RADIANS: return "radians";

		case STEP: return "step";

		case CROSS: return "cross";
		case DOT: return "dot";
		case NORM: return "norm";
		case NORMALIZE: return "normalize";
		case DISTANCE: return "distance";
		case REFLECT: return "reflect";
		case REFRACT: return "refract";

		case OR: return "||";
		case AND: return "&&";
		case NOT: return "!";

		case ADDITION: return "+";
		case SUBTRACTION: return "-";
		case MULTIPLICATION: return "*";
		case QUATERNION_MULTIPLICATION: return "****";
		case DIVISION: return "/";

		case EQUAL: return "==";
		case UNEQUAL: return "!=";

		case LOWER: return "<";
		case LOWER_OR_EQUAL: return "<=";
		case GREATER_OR_EQUAL: return ">=";
		case GREATER: return ">";

		case SIZE: return "#";
		case IN: return "@";
		case SPATIAL_X: return "x";
		case SPATIAL_Y: return "y";
		case SPATIAL_Z: return "z";
		case XY: return "xy";
		case XYZ: return "xyz";

		case SIN: return "sin";
		case COS: return "cos";
		case TAN: return "tan";
		case ASIN: return "asin";
		case ACOS: return "acos";
		case ATAN: return "atan";
		case EXP: return "exp";
		case LOG: return "log";
		case POW: return "pow";
		case SQRT: return "sqrt";
		case RANDOM: return "random";

		case CREATE: return "create";
		case DESTROY: return "destroy";

		case WRITE: return "write";
		case WRITE_ERROR: return "writeError";
		case WRITE_WARNING: return "writeWarning";
		case HALT: return "halt";
		case BREAKPOINT: return "breakpoint";
		case SCREENSHOT: return "screenshot";

		case SET_NUMBER: return "setNumber";
		case SET_COLOR: return "setColor";
		case SET_KEYWORD: return "setProposition";
		case SET_TEXTURE: return "setImage";
		case SET_INTEGER: return "setInteger";
		case GET_NUMBER: return "getNumber";
		case GET_COLOR: return "getColor";
		case GET_KEYWORD: return "getProposition";
		case GET_TEXTURE: return "getImage";
		case GET_INTEGER: return "getInteger";

		case ACTIVATE_ANIMATOR_TRIGGER: return "setTrigger";
		case IN_STATE: return "inState";
		case ACTIVATE_PARAMETER: return "enableParameter";
		case DEACTIVATE_PARAMETER: return "disableParameter";
		case PLAY_ANIMATION: return "playAnimation";

		case EVALUATE: return "evaluate";

		case READ_TRIGGERED: return "readTriggered";
		case READ_NUMBER: return "readNumber";
		case READ_VECTOR: return "readVector";

		case TO_RECTANGLE: return "rectangle";
		case TO_QUATERNION: return "quaternion";
		case TO_NUMBER3: return "angles";

		case PLAY: return "play";
		case PLAY_ONCE: return "playOnce";
		case PAUSE: return "pause";
		case STOP: return "stop";
		case START_RECORDING: return "startRecording";
		case STOP_RECORDING: return "stopRecording";

		case TO_TEXT: return "text";
		case TO_NUMBER: return "number";

		case OVERLAPS: return "overlaps";
		case ADD_FORCE: return "addForce";
		case ADD_TORQUE: return "addTorque";

		case CLOSEST_POINT: return "closestPoint";
		case ANGLE_BETWEEN: return "angleBetween";
		case QUATERNION_BETWEEN: return "quaternionBetween";

		case IS_POSITIVE: return "isPositive";
		case IS_NEGATIVE: return "isNegative";
		case IS_ZERO: return "isZero";

		case WORLD_TO_VIEWPORT: return "worldToViewport";
		case VIEWPORT_TO_WORLD: return "viewportToWorld";

		case ASSIGNMENT: return "=";
		case ABSCISSA: return "abscissa";
		case ALPHA: return "alpha";
		case BLUE: return "blue";
		case DRAW_RAY: return "drawRay";
		case GREEN: return "green";
		case HEIGHT: return "height";
		case HIT_IN: return "@@";
		case HIT_ENTITY: return "hitEntity";
		case HIT_NORMAL: return "hitNormal";
		case HIT_POINT: return "hitPoint";
		case HSLA: return "hsla";
		case HUE: return "hue";
		case LIGHTNESS: return "lightness";
		case ORDINATE: return "ordinate";
		case PLANAR_ADDITION: return "++";
		case PLANAR_DISTANCE: return "planarDistance";
		case PLANAR_DIVISION: return "//";
		case PLANAR_MULTIPLICATION: return "**";
		case PLANAR_NORM: return "planarNorm";
		case PLANAR_NORMALIZE: return "planarNormalize";
		case PLANAR_SUBTRACTION: return "--";
		case PLANAR_X: return "planarX";
		case PLANAR_Y: return "planarY";
		case RAY: return "ray";
		case BOX: return "box";
		case SPHERE: return "sphere";
		case RED: return "red";
		case REMAINDER: return "%";
		case RGBA: return "rgba";
		case SATURATION: return "saturation";
		case SET_TIME_SPEED: return "setTimeSpeed";
		case SPATIAL_ADDITION: return "+++";
		case SPATIAL_DIVISION: return "///";
		case SPATIAL_MULTIPLICATION: return "***";
		case SPATIAL_SUBTRACTION: return "---";
		case WIDTH: return "width";
		case TICK: return "tick";
		case WORLD_POSITION: return "worldPosition";
		case WORLD_ROTATION: return "worldRotation";
		case IS_PARENT: return "isParent";
		case SET_PARENT: return "setParent";
		case CLEAR_PARENT: return "clearParent";
		case SET_WORLD_POSITION: return "setWorldPosition";
		case ENABLE: return "enable";
		case DISABLE: return "disable";
		case BITWISE_AND: return "&";
		case BITWISE_OR: return "|";
		case BITWISE_XOR: return "^";
		case BITWISE_NOT: return "~";
		case SPATIAL_ANGLE: return "spatialAngleBetween";
	} return "";}
	,
	type -> {switch(type)
	{
		case ANYTHING: return "anything";
		case NOTHING: return "nothing";

		case PROPOSITION: return "proposition";
		case NUMBER: return "number";
		case NUMBER2: return "number2";
		case NUMBER3: return "number3";

		case TEXT: return "text";
		case COLOR: return "color";

		case INPUT: return "input";
		case MESH: return "mesh";
		case MATERIAL: return "material";
		case FONT: return "font";
		case AUDIO: return "audio";
		case ANIMATOR: return "animator";
		case CURVE: return "curve";

		case ENTITY: return "entity";
		case ENTITY_LIST: return "entity list";
		case QUATERNION: return "quaternion";
		case IMAGE: return "image";
		case RECTANGLE: return "rectangle";
		case HIT: return "hit";
		case HIT_LIST: return "hit list";
	} return ""; },
	problem -> { switch (problem)
	{
		case SYNTAX_ERROR: return "Syntax error";
		case UNDECIDABLE_TYPE: return "Undecidable type";
		case INCOMPATIBLE_TYPES: return "Incompatible types";
		case UNDEFINED_SYMBOL: return "Undefined symbol";
		case REDEFINED_SYMBOL: return "Redefined symbol";
		case READONLY_ASSIGNMENT: return "Readonly assignment";
		case UNUSED_VALUE: return "Unused value";
	} return ""; },
	bindingReason -> { switch (bindingReason)
	{
		case SAME_VARIABLE: return "Same value";
		case SAME_COMPONENT: return "Same component";
		case SAME_FUNCTION: return "Same function";
		case SAME_TYPE_VARIABLE: return "Same type variable";
	} return ""; },
	typingReason -> { switch (typingReason)
	{
		case STANDARD_VARIABLE: return "Standard value";
		case STANDARD_COMPONENT: return "Standard component";
		case STANDARD_FUNCTION: return "Standard function";
		case STANDARD_BLOCK: return "Standard block";
	} return ""; },
	value -> { switch(value)
	{
		case TRUE: return "A tautology";
		case FALSE: return "A contradiction";
		case EPSILON: return "Smallest real number above zero";
		case PI: return "Ratio between any circle's perimeter and it's diameter";
		case E: return "Value of the natural exponential function at 1";
		case INFINITY: return "A number greater than all numbers";
		case TIME_SINCE_START: return "Time in seconds since the simulation started";
		case DELTA_TIME: return "Time since the last frame";
		case TIME_SPEED: return "Speed factor at which engine system like physics update. Default speed is 1.";
		case FORWARD: return "Unitary positive Z vector: (0,0,1)";
		case RIGHT: return "Unitary positive X vector: (1,0,0)";
		case SCREEN_HEIGHT: return "Main display's height in pixels";
		case SCREEN_WIDTH: return "Main display's width in pixels";
		case UP: return "Unitary positive Y vector: (0,1,0)";
		case ONE: return "The number one";
		case ZERO: return "The number zero";
		case DEFAULT_AUDIO_INPUT: return "The default audio input device set by the user in the operating system";
		case HDMI_AUDIO_INPUT: return "The first audio device whose name includes HDMI";
		case USB_AUDIO_INPUT: return "The first audio device whose name includes USB";
	} return ""; },
	component -> { switch (component)
	{
		case POSITION: return "position";
		case ROTATION: return "rotation";
		case SCALE: return "scale";

		case MASS: return "mass";
		case ANGULAR_MASS: return "inertia";

		case VELOCITY: return "velocity";
		case ANGULAR_VELOCITY: return "angularVelocity";
		case KINEMATIC: return "kinematic";

		case RADIUS: return "radius";
		case EXTENTS: return "extents";
		case BOX_CENTER: return "boxCenter";
		case SPHERE_CENTER: return "sphereCenter";

		case MESH: return "mesh";
		case MATERIAL: return "material";

		case NEAR_PLANE: return "near";
		case FAR_PLANE: return "far";
		case FIELD_OF_VIEW: return "fieldOfView";
		case BACKGROUND: return "background";
		case DISPLAY: return "display";

		case EMISSION: return "emission";
		case SPOT_ANGLE: return "spotAngle";
		case RANGE: return "range";
		case INTENSITY: return "intensity";
		case BOUNCE_INTENSITY: return "bounceIntensity";
		case COOKIE: return "cookie";

		case TEXT: return "text";
		case FONT: return "font";
		case TEXT_MATERIAL: return "textMaterial";

		case IMAGE: return "image";
		case IMAGE_MATERIAL: return "imageMaterial";

		case AUDIO_CLIP: return "audioClip";
		case VOLUME: return "volume";
		case PITCH: return "pitch";
		case LOOP: return "loop";

		case DESTINATION: return "the target position of the entity";
		case MAX_ACCELERATION: return "The maximum acceleration of the entity";
		case MAX_ANGULAR_SPEED: return "The maximum angular speed of the entity";
		case MAX_SPEED: return "The maximum linear speed of the entity";
		case TRAVERSABLE_AREAS: return "The bit field value representing the indices of the areas the entity can traverse";
		case ANIMATOR_CONTROLLER: return "The controller that guides the animation state machine";
		case COOKIE_SIZE: return "The size of the spot light cookie";
		case MUTE: return "Is the audio source muted";
		case PERSPECTIVE: return "Has the camera perspective projection";
		case SCREEN_POSITION: return "The rectangular space assigned to a UI element";
		case VIEWPORT: return "The rectangular space of the display where the camera will render its output";
	} return ""; },
	function ->	{ switch(function)
	{
		case ABS: return "Absolute value of a number";
		case ACOS: return "Arc cosine of a number";
		case ACTIVATE_PARAMETER: return "Enable the parameter in the animator";
		case ADDITION: return "Adds two numbers or vectors";
		case ADD_FORCE: return "Add force to the entity's rigid body";
		case ADD_TORQUE: return "Add torque to the entity's rigid body";
		case AND: return "Logical and of two propositions";
		case ASIN: return "Arc sine of a number";
		case ASSIGNMENT: return "Assigns the expression to the atom";
		case ATAN: return "Arc tangent of a number";
		case BREAKPOINT: return "Pause execution when reached this point";
		case CEIL: return "The closest integer greater or equal to the given number";
		case CLAMP: return "Clamp the number if it exceeds the low and high limits";
		case CLOSEST_POINT: return "Closest point of another collider from the entity's colliders";
		case COS: return "Cosine of the angle";
		case CREATE: return "Create a copy of the entity";
		case CROSS: return "Cross product of the two vectors";
		case DEACTIVATE_PARAMETER: return "Disable the parameter in the animator";
		case DEGREES: return "Convert from radians to degrees";
		case DESTROY: return "Destroy the entity";
		case DISTANCE: return "Distance between the two points";
		case DIVISION: return "Division of the two numbers";
		case DOT: return "Dot product of the two vectors";
		case EQUAL: return "True if both values are equal";
		case EXP: return "e raised to the number";
		case FLOOR: return "Highest integer lower or equal to the number";
		case FRACTIONAL_PART: return "Fractional part of the number";
		case GET_COLOR: return "Value of the color property of the material";
		case GET_INTEGER: return "Value of the integer property of the material";
		case GET_KEYWORD: return "Value of the boolean property of the material";
		case GET_NUMBER: return "Value of the number property of the material";
		case GET_TEXTURE: return "Value of the texture property of the material";
		case GREATER: return "True if the first number is higher than the second";
		case GREATER_OR_EQUAL: return "True if the first number is higher or equal to the second";
		case HALT: return "Stop the execution";
		case IN: return "True if the entity belongs to the entity list";
		case UNEQUAL: return "True if the values are not equal";
		case INTEGER_PART: return "Integer part of the number";
		case INVERSE: return "Multiplicative inverse of the number";
		case IN_STATE: return "True if the animator of the entity is in the state";
		case IS_NEGATIVE: return "True if the number is lower than zero";
		case IS_POSITIVE: return "True if the number is higher than zero";
		case IS_ZERO: return "True if the number is zero";
		case LINEAR_INTERPOLATION: return "Linearly interpolate the number between the extents";
		case LOG: return "Natural logarithm of the number";
		case LOWER: return "True if the first number is lower than the second";
		case LOWER_OR_EQUAL: return "True if the first number is lower or equal to the second";
		case MAX: return "Maximum of the two numbers";
		case MIN: return "Minimum of the two numbers";
		case MULTIPLICATION: return "Multiplication of the two numbers or the vector and the number";
		case QUATERNION_MULTIPLICATION: return "Multiplication of the quaternion and the vector";
		case NORM: return "Length of the vector";
		case NORMALIZE: return "Normalized version of the vector";
		case NOT: return "Negation of the proposition";
		case OR: return "Inclusive disjunction of the propositions";
		case OVERLAPS: return "List of all the entities overlapping the entity's colliders";
		case PAUSE: return "Pause the audio";
		case PLAY: return "Play the audio";
		case PLAY_ONCE: return "Play the audio once";
		case PLAY_ANIMATION: return "Play the animation state";
		case EVALUATE: return "Evaluate the curve at the given time";
		case POW: return "The first number raised to the second";
		case PROPORTIONAL: return "The proportional value of the number when shifted from the original extents to the destination extents";
		case RADIANS: return "Convert the degrees to radians";
		case RANDOM: return "A random number between the minimum and the maximum numbers";
		case READ_NUMBER: return "Numerical value of the input device";
		case READ_TRIGGERED: return "True if the input device has been triggered this frame";
		case READ_VECTOR: return "Vector value of the input device";
		case RECIPROCAL: return "Additive inverse of the number";
		case REFLECT: return "Reflection of the vector";
		case REFRACT: return "Refraction of the vector when changes to the new environment";
		case ROUND: return "The number rounded up or down";
		case SCREENSHOT: return "Take a screenshot";
		case SET_COLOR: return "Set the color property of the material";
		case SET_INTEGER: return "Set the integer property of the material";
		case SET_KEYWORD: return "Set the boolean property of the material";
		case SET_NUMBER: return "Set the number property of the material";
		case SET_TEXTURE: return "Set the texture property of the material";
		case ACTIVATE_ANIMATOR_TRIGGER: return "Set the property of the material";
		case SIGN: return "1 if the number is positive or zero, -1 otherwise";
		case SIN: return "Sine of the angle";
		case SIZE: return "Size of the list of entities";
		case SPHERICAL_INTERPOLATION: return "Spherically interpolate between the two quaternions";
		case SQRT: return "Square root of the number";
		case STEP: return "Value of the step function at the number";
		case STOP: return "Stop the audio";
		case SUBTRACTION: return "Subtraction of the two numbers or vectors";
		case TAN: return "Tangent of the angle";
		case TO_NUMBER: return "Convert the string to a number";
		case TO_NUMBER3: return "Convert the quaternion to a 3D vector";
		case TO_QUATERNION: return "Convert the 3D vector to a quaternion";
		case TO_TEXT: return "Convert the number to a string";
		case INVERSE_LINEAR_INTERPOLATION: return "Inverse of interpolating the number between the extents";
		case VIEWPORT_TO_WORLD: return "Convert the viewport coordinates to world coordinates";
		case WORLD_TO_VIEWPORT: return "Convert world coordinates to viewport coordinates";
		case WRITE: return "Write a log message with the value";
		case WRITE_ERROR: return "Write an error message with the value";
		case WRITE_WARNING: return "Write a warning message with the value";
		case SPATIAL_X: return "First entry of the vector";
		case XY: return "2D vector with the two numbers as entries";
		case XYZ: return "3D vector with the three numbers as entries";
		case SPATIAL_Y: return "Second entry of the vector";
		case SPATIAL_Z: return "Third entry of the vector";
		case ANGLE_BETWEEN: return "Angle between two 2D vectors";
		case QUATERNION_BETWEEN: return "Quaternion representing the rotation taking the first vector to the second";
		case TO_RECTANGLE: return "Create a rectangle starting at (x,y) with width z and height w";
		case ABSCISSA: return "The X coordinate where the rectangle starts";
		case ALPHA: return "The alpha channel of the color";
		case BLUE: return "The blue channel of the color";
		case DRAW_RAY: return "Draw a ray for debugging purposes";
		case GREEN: return "The green channel of the color";
		case HEIGHT: return "The height of the rectangle";
		case HIT_IN: return "Does the hit belong to the list of hits";
		case HIT_ENTITY: return "The entity where the hit was produced";
		case HIT_NORMAL: return "The normal vector of the surface where the hit was produced";
		case HIT_POINT: return "The point where the hit was produced";
		case HSLA: return "Create a color from hue, saturation, lightness and alpha";
		case HUE: return "The hue channel of the color in HSLA format";
		case LIGHTNESS: return "The lightness channel of the color in HSLA format";
		case ORDINATE: return "The Y coordinate where the rectangle starts";
		case PLANAR_ADDITION: return "Component-wise addition over 2D vectors";
		case PLANAR_DISTANCE: return "The distance between two 2D points";
		case PLANAR_DIVISION: return "Component-wise division of the 2D vector by the number";
		case PLANAR_MULTIPLICATION: return "Component-wise multiplication of the 2D vector by the number";
		case PLANAR_NORM: return "The magnitude of the 2D vector";
		case PLANAR_NORMALIZE: return "The normalized equivalent of the 2D vector";
		case PLANAR_SUBTRACTION: return "Component-wise subtraction over 2D vectors";
		case PLANAR_X: return "The X coordinate of a 2D vector";
		case PLANAR_Y: return "The Y coordinate of a 2D vector";
		case RAY: return "Cast a ray from the origin in the direction for the given distance. Returns a list of all the hits.";
		case BOX: return "Cast a box from the origin of the size in the given rotation. Returns a list of all the hits";
		case SPHERE: return "Cast a sphere from the origin of the given radius. Returns a list of all the hits";
		case RED: return "The red channel of the color";
		case REMAINDER: return "The remainder of the division between the numbers";
		case RGBA: return "Create a color from red, green, blue and alpha channels";
		case SATURATION: return "The saturation channel of the color in HSLA format";
		case SET_TIME_SPEED: return "Set the factor by which the time is multiplied. Default is one.";
		case SPATIAL_ADDITION: return "Component-wise addition over 3D vectors";
		case SPATIAL_DIVISION: return "Component-wise division over the 3D vector by the number";
		case SPATIAL_MULTIPLICATION: return "Component-wise multiplication over the 3D vector by the number";
		case SPATIAL_SUBTRACTION: return "Component-wise subtraction over 3D vectors";
		case WIDTH: return "The width of the rectangle";
		case TICK: return "Decrease the number by deltaTime: the time elapsed since last frame. Useful for ticking timers";
		case WORLD_POSITION: return "The position of the entity in world coordinates";
		case WORLD_ROTATION: return "The rotation of the entity in world coordinates";
		case IS_PARENT: return "Is the first entity the parent of the second entity";
		case SET_PARENT: return "Set the first entity as the parent of the second entity";
		case CLEAR_PARENT: return "Set the scene as the parent of the entity";
		case SET_WORLD_POSITION: return "Set the position of the entity in world coordinates";
		case ENABLE: return "Enable the entity";
		case DISABLE: return "Disable the entity";
		case BITWISE_AND: return "The bitwise and of the two numbers";
		case BITWISE_OR: return "The bitwise or of the two numbers";
		case BITWISE_XOR: return "The bitwise xor or the two numbers";
		case BITWISE_NOT: return "The bitwise not of the number";
		case START_RECORDING: return "Start recording audio from the audio device to the resulting audio clip";
		case STOP_RECORDING: return "Finish recording from the audio device";
		case SPATIAL_ANGLE: return "The angle between the two spatial vectors";
	}
	return "";},
	classification -> { switch(classification)
	{
		case QUERY_ENTITY: return "Query entity";
		case USER_VALUE: return "User value";
		case USER_COMPONENT: return "User component";
		case USER_SYSTEM: return "User system";

		case STANDARD_VALUE: return "Standard value";
		case STANDARD_COMPONENT: return "Standard component";
		case STANDARD_OPERATOR: return "Standard operator";
		case STANDARD_FUNCTION: return "Standard function";
	}
	return "";}
	);

	Map<Value, String> valueToName;
	Map<Component, String> componentToName;
	Map<Function, String> functionToName;
	Map<AtomicType, String> typeToName;
	Map<ProblemKind, String> problemToName;
	Map<BindingReason, String> bindingReasonToName;
	Map<TypingReason, String> typingReasonToName;

	Map<Value, String> valueToDescription;
	Map<Component, String> componentToDescription;
	Map<Function, String> functionToDescription;
	Map<Classification, String> classificationToDescription;

	Map<String, Value> nameToValue;
	Map<String, Component> nameToComponent;
	Map<String, Function> nameToFunction;
	Map<String, AtomicType> nameToType;

	public final String query;
	public final String selection;
	public final String iteration;

	Library(String query, String selection, String iteration, java.util.function.Function<Value, String> values, java.util.function.Function<Component, String> components, java.util.function.Function<Function, String> functions, java.util.function.Function<AtomicType, String> atomicTypes, java.util.function.Function<ProblemKind, String> problems, java.util.function.Function<BindingReason, String> bindingReasons, java.util.function.Function<TypingReason, String> typingReasons, java.util.function.Function<Value, String> valueDescriptions, java.util.function.Function<Component, String> componentDescriptions, java.util.function.Function<Function, String> functionDescriptions, java.util.function.Function<Classification,String> classifications)
	{
		this.query = query;
		this.selection = selection;
		this.iteration = iteration;

		valueToName = forward(values, Value.values());
		nameToValue = reverse(values, Value.values());

		componentToName = forward(components, Component.values());
		nameToComponent = reverse(components, Component.values());

		functionToName = forward(functions, Function.values());
		nameToFunction = reverse(functions, Function.values());

		typeToName = forward(atomicTypes, AtomicType.values());
		nameToType = reverse(atomicTypes, AtomicType.values());

		problemToName = forward(problems, ProblemKind.values());
		bindingReasonToName = forward(bindingReasons, BindingReason.values());
		typingReasonToName = forward(typingReasons, TypingReason.values());

		valueToDescription = forward(valueDescriptions, Value.values());
		componentToDescription = forward(componentDescriptions, Component.values());
		functionToDescription = forward(functionDescriptions, Function.values());

		classificationToDescription = forward(classifications, Classification.values());
	}

	private <A,B> HashMap<A,B> forward(java.util.function.Function<A,B> f, A[] values)
	{
		var map = new HashMap<A,B>();

		for (var value : values)
		{
			map.put(value, f.apply(value));
		}

		return map;
	}

	private <A,B> HashMap<B,A> reverse(java.util.function.Function<A,B> f, A[] values)
	{
		var map = new HashMap<B,A>();

		for (var value : values)
		{
			var mapped = f.apply(value);

			if (!map.containsKey(mapped))
			{
				map.put(mapped, value);
			}
		}

		return map;
	}

	public Value getValue(String name)
	{
		return nameToValue.get(name);
	}

	public Component getComponent(String name)
	{
		return nameToComponent.get(name);
	}

	public Function getFunction(String name)
	{
		return nameToFunction.get(name);
	}

	public String getProblem(ProblemKind problem)
	{
		return problemToName.get(problem);
	}

	public String getDescription(Function function)
	{
		return functionToDescription.get(function);
	}

	public String getDescription(Component component)
	{
		return componentToDescription.get(component);
	}

	public String getDescription(Value value)
	{
		return valueToDescription.get(value);
	}

	public String getDescription(Classification classification)
	{
		return classificationToDescription.get(classification);
	}

	public String getName(Component component)
	{
		return componentToName.get(component);
	}

	public String getName(Symbol symbol)
	{
		if (symbol instanceof Value)
		{
			return getName((Value) symbol);
		}
		else if (symbol instanceof Component)
		{
			return getName((Component) symbol);
		}
		else if (symbol instanceof Function)
		{
			return getName((Function) symbol);
		}
		return "?";
	}

	public String getName(Function function)
	{
		return functionToName.get(function);
	}

	public String getName(Value value)
	{
		return valueToName.get(value);
	}

	public String getName(Type type)
	{
		if (type instanceof AtomicType)
		{
			var atomicType = (AtomicType) type;

			return typeToName.get(atomicType);
		}
		else if (type instanceof TypeVariable)
		{
			return "T";
		}
		else if (type instanceof FunctionType)
		{
			var functionType = (FunctionType) type;
			var result = new ArrayList<String>();
			for (var p : functionType.parameterTypes)
			{
				result.add(getName(p));
			}
			return String.join(" × ", result) + " -> " + getName(functionType.returnType);
		}
		else
		{
			return "";
		}
	}
}
