package m.library.symbols;

import static m.library.types.AtomicType.NUMBER;
import static m.library.types.AtomicType.NUMBER3;
import static m.library.types.AtomicType.PROPOSITION;
import static m.library.types.AtomicType.TEXT;

import m.library.types.Type;

public enum Value implements Symbol
{
	TRUE(PROPOSITION),
	FALSE(PROPOSITION),

	EPSILON(NUMBER),
	PI(NUMBER),
	E(NUMBER),
	INFINITY(NUMBER),

	ZERO(NUMBER),
	ONE(NUMBER),

	UP(NUMBER3),
	RIGHT(NUMBER3),
	FORWARD(NUMBER3),

	SCREEN_WIDTH(NUMBER),
	SCREEN_HEIGHT(NUMBER),

	DELTA_TIME(NUMBER),
	TIME_SINCE_START(NUMBER),
	TIME_SPEED(NUMBER),

	DEFAULT_AUDIO_INPUT(TEXT),
	HDMI_AUDIO_INPUT(TEXT),
	USB_AUDIO_INPUT(TEXT),
	;

	Type type;

	Value (Type type)
	{
		this.type = type;
	}

	public Type getType()
	{
		return type;
	}
}
