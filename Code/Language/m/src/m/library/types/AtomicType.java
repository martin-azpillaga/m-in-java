package m.library.types;

// TODO Component type for adding and removing components
public enum AtomicType implements Type
{
	NOTHING,
	ANYTHING,

	PROPOSITION,
	NUMBER,
	NUMBER2,
	NUMBER3,
	QUATERNION,

	COLOR,
	RECTANGLE,
	HIT,

	ENTITY,

	CURVE,
	TEXT,
	INPUT,
	IMAGE,
	MESH,
	MATERIAL,
	FONT,
	AUDIO,
	ANIMATOR,

	HIT_LIST,
	ENTITY_LIST
	;
}
