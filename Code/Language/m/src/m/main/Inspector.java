package m.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.lsp4j.CompletionItem;
import org.eclipse.lsp4j.CompletionItemKind;
import org.eclipse.lsp4j.Location;
import org.eclipse.lsp4j.Position;
import org.eclipse.lsp4j.Range;
import org.eclipse.lsp4j.SemanticTokens;
import org.eclipse.lsp4j.SignatureHelp;
import org.eclipse.lsp4j.SignatureInformation;
import org.eclipse.lsp4j.SymbolInformation;
import org.eclipse.lsp4j.SymbolKind;
import org.eclipse.lsp4j.TextEdit;
import org.eclipse.lsp4j.WorkspaceEdit;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.TerminalRule;
import org.eclipse.xtext.impl.KeywordImpl;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.nodemodel.impl.CompositeNode;
import org.eclipse.xtext.nodemodel.impl.CompositeNodeWithSemanticElement;
import org.eclipse.xtext.nodemodel.impl.HiddenLeafNode;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;

import m.library.Library;
import m.library.rules.Classification;
import m.library.symbols.Component;
import m.library.types.AtomicType;
import m.model.Application;
import m.model.Assignment;
import m.model.Binary;
import m.model.BindingBlock;
import m.model.Block;
import m.model.Cell;
import m.model.Delegation;
import m.model.Expression;
import m.model.File;
import m.model.Function;
import m.model.Game;
import m.model.Statement;
import m.model.Unary;
import m.model.UserFunction;
import m.model.Value;

import static m.generator.IO.*;
import static m.library.rules.Classification.*;

public class Inspector
{
	Library library;
	Game game;
	Map<String,INode> files;

	public Inspector()
	{
		this.files = new HashMap<>();
		this.library = Library.ENGLISH;
	}

	public SemanticTokens semanticTokens(String file)
	{
		var data = new ArrayList<Integer>();
		var result = new SemanticTokens(data);

		var root = files.get(file).getLeafNodes();

		Integer newLines = 0;
		var indent = 0;
		var lastLength = 0;

		for (var leaf : root)
		{
			if (leaf.getGrammarElement() instanceof KeywordImpl)
			{
				data.add(newLines == null ? 0 : newLines);
				data.add(newLines == null ? lastLength : indent);
				data.add(leaf.getLength());
				data.add(2);
				data.add(0);

				newLines = null;
			}
			else if (leaf instanceof HiddenLeafNode)
			{
				var split = leaf.getText().split("\n", -1);
				newLines = split.length - 1;
				indent = split[split.length-1].length();
				if (newLines == 0)
				{
					indent += lastLength;
				}
			}
			else
			{
				data.add(newLines == null ? 0 : newLines);
				data.add(newLines == null ? lastLength : indent);
				data.add(leaf.getLength());

				var semantic = leaf.getSemanticElement();
				if (semantic instanceof Function)
				{
					data.add(0);
				}
				else if (semantic instanceof BindingBlock)
				{
					data.add(2);
				}
				else if (semantic instanceof Block)
				{
					data.add(2);
				}
				else if (semantic instanceof Application)
				{
					data.add(1);
				}
				else if (semantic instanceof Value)
				{
					var parent = leaf.getParent().getParent().getSemanticElement();
					if (parent instanceof Cell)
					{
						var cell = (Cell) parent;
						if (semantic == cell.getEntity())
						{
							data.add(5);
						}
						else
						{
							data.add(6);
						}
					}
					else
					{
						data.add(5);
					}
				}
				else if (semantic instanceof Binary)
				{
					data.add(3);
				}
				else if (semantic instanceof Unary)
				{
					data.add(3);
				}
				else
				{
					data.add(1);
				}

				data.add(0);

				newLines = null;
			}

			lastLength = leaf.getLength();
		}

		return result;
	}

	public List<Location> references(String file, Position position)
	{
		var result = new ArrayList<Location>();

		var node = nodeAt(file, position, false);
		if (node.getSemanticElement() instanceof Value)
		{
			var parent = node.getParent().getParent();

			if (parent.getSemanticElement() instanceof Cell)
			{
				var cell = (Cell) parent.getSemanticElement();
				if (cell.getComponent() == node.getSemanticElement())
				{
					var root = files.get(file);
					for (var component : EcoreUtil2.getAllContentsOfType(root.getSemanticElement(), Cell.class))
					{
						if (component.getComponent().getName().equals(((Value)node.getSemanticElement()).getName()))
						{
							var location = new Location(file, rangeOf(NodeModelUtils.findActualNodeFor(component), root.getText()));
							result.add(location);
						}
					}
				}
			}
			else
			{

			}
		}

		return result;
	}

	public WorkspaceEdit rename(String file, Position position, String newName)
	{
		var node = nodeAt(file, position, false);
		if (node.getSemanticElement() instanceof Value)
		{
			var result = new WorkspaceEdit();
			var changes = new HashMap<String, List<TextEdit>>();
			result.setChanges(changes);
			var fileChanges = new ArrayList<TextEdit>();
			changes.put(file, fileChanges);
			for (var value : EcoreUtil2.getAllContentsOfType(files.get(file).getSemanticElement(), Value.class))
			{
				if (value.getName().equals(node.getText()))
				{
					var valueNode = NodeModelUtils.findActualNodeFor(value);
					var edit = new TextEdit(rangeOf(valueNode, files.get(file).getText()), newName);
					fileChanges.add(edit);
				}
			}

			return result;
		}
		else
		{
			return new WorkspaceEdit();
		}
	}

	private Range rangeOf(INode node, String text)
	{
		var range = new Range();

		var offset = node.getOffset();

		var line = node.getStartLine();

		var count = 0;
		for (var i = 0; i < text.length(); i++)
		{
			if (count == line-1)
			{
				return new Range(new Position(line-1, offset-i),new Position(line-1, offset-i+node.getLength()));
			}
			if (text.charAt(i) == '\n')
			{
				count++;
			}
		}

		return range;
	}

	public List<SymbolInformation> symbols()
	{
		var result = new ArrayList<SymbolInformation>();
		for (var file : files.keySet())
		{
			result.addAll(symbols(file));
		}
		return result;
	}

	public List<SymbolInformation> symbols(String file)
	{
		var result = new ArrayList<SymbolInformation>();

		var root = files.get(file);

		var model = (File) root.getSemanticElement();

		if (model == null) return new ArrayList<>();

		for (var function : model.getFunctions())
		{
			var start = new Position(NodeModelUtils.findActualNodeFor(function).getStartLine()-1,0);
			var end = new Position(NodeModelUtils.findActualNodeFor(function).getEndLine()-1,0);
			result.add(new SymbolInformation(function.getName(), SymbolKind.Function, new Location(file, new Range(start,end))));
		}

		return result;
	}

	public TextEdit format(String file)
	{
		var root = files.get(file);

		var model = (File) root.getSemanticElement();

		var text = write(lines
		(
			foreach(model.getFunctions(), function->lines
			(
				function.getName(),
				"{",
					foreach(function.getStatements(), s->format(s)),
				"}"
			))
		));

		var lines = root.getText().split("\n");

		return new TextEdit(new Range(new Position(0, 0), new Position(lines.length+1,0)),text);
	}

	private Object format(Statement statement)
	{
		if (statement instanceof BindingBlock)
		{
			var block = (BindingBlock) statement;
			return lines
			(
				block.getName()+" "+format(block.getExpression())+":",
				"{",
					foreach(block.getStatements(), s->format(s)),
				"}"
			);
		}
		else if (statement instanceof Block)
		{
			var block = (Block) statement;
			return lines
			(
				block.getName()+" "+format(block.getExpression()),
				"{",
					foreach(block.getStatements(), s->format(s)),
				"}"
			);
		}
		else if (statement instanceof Assignment)
		{
			var assignment = (Assignment) statement;
			return format(assignment.getAtom())+" = "+format(assignment.getExpression());
		}
		else if (statement instanceof Delegation)
		{
			var delegation = (Delegation) statement;
			return format(delegation.getApplication());
		}
		return "not formattable";
	}

	private String format(Expression expression)
	{
		if (expression instanceof Unary)
		{
			var unary = (Unary) expression;
			return unary.getOperator()+format(unary.getExpression());
		}
		else if (expression instanceof Binary)
		{
			var binary = (Binary) expression;
			return format(binary.getLeft())+" "+binary.getOperator()+" "+format(binary.getRight());
		}
		else if (expression instanceof Application)
		{
			var application = (Application) expression;
			return application.getName()+"("+foreach(application.getArguments(),a->format(a),", ")+")";
		}
		else if (expression instanceof Value)
		{
			var value = (Value) expression;
			return value.getName();
		}
		else if (expression instanceof Cell)
		{
			var cell = (Cell) expression;
			return format(cell.getEntity())+"."+format(cell.getComponent());
		}
		return "not formattable";
	}

	public void update(String file, INode node, Game game)
	{
		files.put(file,node);
		this.game = game;
	}

	public void delete(String file)
	{
		files.remove(file);
	}

	public void update(Game game)
	{
		this.game = game;
	}

	public String hover(String file, Position position)
	{
		var node = nodeAt(file, position, false);
		var semantic = node.getSemanticElement();

		if (semantic instanceof Function)
		{
			var function = (Function) semantic;
			if (node.getText().equals(function.getName()))
			{
				return library.getDescription(Classification.USER_SYSTEM);
			}
		}
		else if (semantic instanceof Value)
		{
			var value = (Value) semantic;

			var container = value.eContainer();

			if (container instanceof Cell)
			{
				var cell = (Cell) container;
				if (cell.getComponent() == value)
				{
					var standard = library.getComponent(value.getName());

					if (standard != null)
					{
						var type = standard.getType();
						return library.getDescription(STANDARD_COMPONENT) + " : " + library.getName(type);
					}
					else
					{
						var type = game.components.get(value.getName());
						if (type != null)
						{
							return library.getDescription(USER_COMPONENT) + " : " + library.getName(type);
						}
						else
						{
							return library.getDescription(USER_COMPONENT) + " : ?";
						}
					}
				}
				else
				{
					var function = EcoreUtil2.getContainerOfType(cell, Function.class);
					UserFunction userFunction = null;
					for (var f : game.functions)
					{
						if (f.function==function)
						{
							userFunction = f;
							break;
						}
					}
					var query = userFunction.roles.get(value.getName());

					if (query == null)
					{
						var standard = library.getValue(value.getName());

						if (standard != null)
						{
							var type = library.getName(standard.getType());
							return library.getDescription(STANDARD_VALUE)+ " : " + type;
						}
						else
						{
							var type = game.values.get(value);
							if (type != null)
							{
								return library.getDescription(USER_VALUE) + " : " + library.getName(type);
							}
							else
							{
								return library.getDescription(USER_VALUE) + " : ?";
							}
						}
					}
					else
					{
						var builder = new StringBuilder();
						builder.append(library.getDescription(QUERY_ENTITY)+"\n\n");
						for (var component : query.entrySet())
						{
							builder.append("* ");
							builder.append(component.getKey());
							builder.append("\n\n");
						}
						return builder.toString();
					}
				}
			}
			else
			{
				var function = EcoreUtil2.getContainerOfType(value, Function.class);
				UserFunction userFunction = null;
				for (var f : game.functions)
				{
					if (f.getName().equals(function.getName()))
					{
						userFunction = f;
						break;
					}
				}
				var query = userFunction.roles.get(value.getName());

				if (query == null)
				{
					var standard = library.getValue(value.getName());

					if (standard != null)
					{
						var type = library.getName(standard.getType());
						return library.getDescription(STANDARD_VALUE) + " : " + type;
					}
					else
					{
						var type = game.values.get(value);
						if (type != null)
						{
							return library.getDescription(USER_VALUE) + " : " + library.getName(type);
						}
						else
						{
							return library.getDescription(USER_VALUE) + " : ?";
						}
					}
				}
				else
				{
					var builder = new StringBuilder();
					builder.append(library.getDescription(QUERY_ENTITY)+"\n\n");
					for (var component : query.entrySet())
					{
						builder.append("* ");
						builder.append(component.getKey());
						builder.append("\n\n");
					}
					return builder.toString();
				}
			}
		}
		else if (semantic instanceof Binary)
		{
			var binary = (Binary) semantic;
			var standard = library.getFunction(binary.getOperator());
			if (standard != null)
			{
				return library.getDescription(STANDARD_OPERATOR) + " : " + library.getName(standard.getType());
			}
		}
		else if (semantic instanceof Unary)
		{
			var unary = (Unary) semantic;
			var standard = library.getFunction(unary.getOperator());
			if (standard != null)
			{
				return library.getDescription(STANDARD_OPERATOR) + " : " + library.getName(standard.getType());
			}
		}
		else if (semantic instanceof Application)
		{
			var application = (Application) semantic;
			var standard = library.getFunction(application.getName());
			if (standard != null)
			{
				return library.getDescription(STANDARD_OPERATOR) + " : " + library.getName(standard.getType());
			}
		}

		return "";
	}

	public List<CompletionItem> completions(String file, Position position)
	{
		var result = new ArrayList<CompletionItem>();

		var node = nodeAt(file, position, true);

		var semantic = node.getSemanticElement();
		var grammatical = node.getGrammarElement();

		if (semantic instanceof Cell || semantic instanceof Value && semantic.eContainer() instanceof Cell)
		{
			Cell cell;
			if (semantic instanceof Cell)
			{
				cell = (Cell) semantic;
			}
			else
			{
				cell = (Cell) semantic.eContainer();
			}

			for (var component : game.components.keySet())
			{
				if (component == null || cell.getComponent() != null && cell.getComponent().getName() != null && cell.getComponent().getName().equals(component))
				{
					continue;
				}
				if (library.getComponent(component) != null)
				{
					continue;
				}
				var item = new CompletionItem(component);
				item.setDocumentation(library.getDescription(library.getComponent(component)));
				item.setDetail(library.getName(game.components.get(component)));
				item.setKind(CompletionItemKind.Class);
				result.add(item);
			}
			for (var component : Component.values())
			{
				var item = new CompletionItem(library.getName(component));
				item.setDocumentation(library.getDescription(component));
				item.setDetail(library.getName(component.getType()));
				item.setKind(CompletionItemKind.Enum);
				result.add(item);
			}
		}
		else if (grammatical instanceof TerminalRule && ((TerminalRule)grammatical).getName().equals("IDENTIFIER"))
		{
			for (var function : m.library.symbols.Function.values())
			{
				if (function != m.library.symbols.Function.ASSIGNMENT)
				{
					var item = new CompletionItem(library.getName(function));
					item.setKind(CompletionItemKind.Function);
					result.add(item);
				}
			}

			for (var value : m.library.symbols.Value.values())
			{
				var item = new CompletionItem(library.getName(value));
				item.setKind(CompletionItemKind.Value);
				result.add(item);
			}

			EObject statement = EcoreUtil2.getContainerOfType(semantic, Statement.class);
			if (statement == null)
			{
				statement = EcoreUtil2.getContainerOfType(semantic, Function.class);
			}
			if (statement == semantic)
			{
				if (statement instanceof BindingBlock)
				{
					var name = ((BindingBlock) statement).getExpression().getName();
					var item = new CompletionItem(name);
					item.setDetail(library.getName(AtomicType.ENTITY));
					item.setSortText("0"+name);
					item.setKind(CompletionItemKind.Variable);
					result.add(item);
				}

				var siblings = node.getParent().getParent().getChildren();
				for (var sibling : siblings)
				{
					if (sibling == node.getParent())
					{
						break;
					}
					else if (sibling instanceof CompositeNode && ((CompositeNode)sibling).getFirstChild() instanceof CompositeNodeWithSemanticElement)
					{
						var semanticOfSibling = ((CompositeNode)sibling).getFirstChild().getSemanticElement();
						if (semanticOfSibling instanceof Assignment)
						{
							var assignment = (Assignment) semanticOfSibling;
							if (assignment.getAtom() instanceof Value)
							{
								var atom = (Value) assignment.getAtom();

								var item = new CompletionItem(atom.getName());
								item.setKind(CompletionItemKind.Variable);
								result.add(item);
							}
						}
					}
				}
			}
			var container = statement.eContainer();

			while(! (container instanceof m.model.File))
			{
				if (container instanceof BindingBlock)
				{
					var name = ((BindingBlock) container).getExpression().getName();
					var item = new CompletionItem(name);
					item.setDetail(library.getName(AtomicType.ENTITY));
					item.setSortText("0"+name);
					item.setKind(CompletionItemKind.Variable);
					result.add(item);
				}

				List<Statement> statements;

				if (container instanceof Block)
				{
					statements = ((Block) container).getStatements();
				}
				else if (container instanceof BindingBlock)
				{
					statements = ((BindingBlock) container).getStatements();
				}
				else if (container instanceof Function)
				{
					statements = ((Function) container).getStatements();
				}
				else
				{
					statements = new ArrayList<>();
				}

				for (var s : statements)
				{
					if (s == statement)
					{
						break;
					}
					else
					{
						if (s instanceof Assignment)
						{
							var assignment = (Assignment) s;
							if (assignment.getAtom() instanceof Value)
							{
								var atom = (Value) assignment.getAtom();

								var item = new CompletionItem(atom.getName());
								item.setKind(CompletionItemKind.Variable);
								item.setSortText("0"+atom.getName());
								result.add(item);
							}
						}
					}
				}

				var temp = container.eContainer();
				statement = container;
				container = temp;
			}
		}
		else if (semantic instanceof Value)
		{
			var semanticContainer = semantic.eContainer();
			if (semanticContainer instanceof BindingBlock)
			{
				var bindingBlock = (BindingBlock) semanticContainer;
				if (bindingBlock.getExpression() == semantic)
				{
					return result;
				}
			}
			for (var function : m.library.symbols.Function.values())
			{
				if (function != m.library.symbols.Function.ASSIGNMENT)
				{
					var item = new CompletionItem(library.getName(function));
					item.setKind(CompletionItemKind.Function);
					result.add(item);
				}
			}

			for (var value : m.library.symbols.Value.values())
			{
				var item = new CompletionItem(library.getName(value));
				item.setKind(CompletionItemKind.Value);
				result.add(item);
			}

			EObject statement = EcoreUtil2.getContainerOfType(semantic, Statement.class);
			var container = statement.eContainer();

			while(! (container instanceof m.model.File))
			{
				if (container instanceof BindingBlock)
				{
					var name = ((BindingBlock) container).getExpression().getName();
					var item = new CompletionItem(name);
					item.setDetail(library.getName(AtomicType.ENTITY));
					item.setSortText("0"+name);
					item.setKind(CompletionItemKind.Variable);
					result.add(item);
				}

				List<Statement> statements;

				if (container instanceof Block)
				{
					statements = ((Block) container).getStatements();
				}
				else if (container instanceof BindingBlock)
				{
					statements = ((BindingBlock) container).getStatements();
				}
				else if (container instanceof Function)
				{
					statements = ((Function) container).getStatements();
				}
				else
				{
					statements = new ArrayList<>();
				}

				for (var s : statements)
				{
					if (s == statement)
					{
						break;
					}
					else
					{
						if (s instanceof Assignment)
						{
							var assignment = (Assignment) s;
							if (assignment.getAtom() instanceof Value)
							{
								var atom = (Value) assignment.getAtom();

								var item = new CompletionItem(atom.getName());
								item.setKind(CompletionItemKind.Variable);
								item.setSortText("0"+atom.getName());
								result.add(item);
							}
						}
					}
				}

				var temp = container.eContainer();
				statement = container;
				container = temp;
			}
		}

		return result;
	}

	public SignatureHelp signatures(String path, Position position, String triggerCharacter)
	{
		var help = new SignatureHelp();

		var node = nodeAt(path, position, true);

		var list = new ArrayList<SignatureInformation>();

		if (node != null && node.getSemanticElement() instanceof Application)
		{
			var application = (Application) node.getSemanticElement();
			var name = application.getName();
			var standard = library.getFunction(name);

			if (standard != null)
			{
				var type = standard.getType();
				var info = new SignatureInformation(name + " : " + library.getName(type), library.getDescription(standard),null);
				list.add(info);
				help.setSignatures(list);
				if (triggerCharacter != null)
				{
					help.setActiveParameter(application.getArguments().size());
				}
				else
				{
					help.setActiveParameter(application.getArguments().size()-1);
				}
			}
		}

		help.setSignatures(list);
		return help;
	}

	private INode nodeAt(String file, Position position, boolean minusOne)
	{
		var rootNode = files.get(file);
		var offset = offset(rootNode.getText(), position.getLine(), position.getCharacter());

		var node = NodeModelUtils.findLeafNodeAtOffset(rootNode, minusOne && offset > 0 ? offset-1 : offset);

		return node;
	}

	private int offset(String text, int line, int character)
	{
		var count = 0;
		for (var i = 0; i < text.length(); i++)
		{
			if (count == line)
			{
				return i+character;
			}
			if (text.charAt(i) == '\n')
			{
				count++;
			}
		}
		return text.length()-1;
	}
}
