package m.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.JsonPrimitive;

import org.eclipse.lsp4j.CompletionItem;
import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.DiagnosticSeverity;
import org.eclipse.lsp4j.Location;
import org.eclipse.lsp4j.Position;
import org.eclipse.lsp4j.Range;
import org.eclipse.lsp4j.SemanticTokens;
import org.eclipse.lsp4j.SignatureHelp;
import org.eclipse.lsp4j.SymbolInformation;
import org.eclipse.lsp4j.TextEdit;
import org.eclipse.lsp4j.WorkspaceEdit;
import org.eclipse.xtext.nodemodel.INode;

import m.generator.Generator;
import m.generator.IO;
import m.validation.Problem;
import m.validation.Validator;

public class Project
{
	String root;

	Validator validator;
	Generator generator;
	Inspector inspector;
	String outputFolder;

	public Project(String root)
	{
		this.root = root;
		this.generator = new Generator();
		this.validator = new Validator();
		this.inspector = new Inspector();
	}

	public Map<String,List<Diagnostic>> initialize()
	{
		Validator.Result data = null;

		for (var file : IO.filesWithExtension("Ⲙ", root))
		{
			var text = IO.read(file);
			data = validator.validate(file,text);
			inspector.update(file, data.rootNode, data.game);
		}

		return check(data);
	}

	public void setConfiguration(List<Object> config)
	{
		outputFolder = ((JsonPrimitive) config.get(0)).getAsString();
	}

	public Map<String,List<Diagnostic>> delete(String file)
	{
		var globalData = validator.delete(file);
		inspector.delete(file);

		return check(globalData);
	}

	public Map<String,List<Diagnostic>> modify(String file, String text)
	{
		var data = validator.validate(file, text);
		inspector.update(file, data.rootNode, data.game);

		return check(data);
	}

	public String hover(String file, Position position)
	{
		return inspector.hover(file, position);
	}

	public List<CompletionItem> completions(String file, Position position)
	{
		return inspector.completions(file, position);
	}

	public SignatureHelp signatures(String file, Position position, String triggerCharacter)
	{
		return inspector.signatures(file, position, triggerCharacter);
	}

	public TextEdit format(String file)
	{
		return inspector.format(file);
	}


	public List<SymbolInformation> symbols(String file)
	{
		return inspector.symbols(file);
	}

	public List<SymbolInformation> symbols()
	{
		return inspector.symbols();
	}

	public WorkspaceEdit rename(String file, Position position, String newName)
	{
		return inspector.rename(file, position, newName);
	}

	public SemanticTokens semanticTokens(String file)
	{
		return inspector.semanticTokens(file);
	}

	public List<Location> references(String file, Position position)
	{
		return inspector.references(file, position);
	}



	private Map<String,List<Diagnostic>> check(Validator.Result validationResult)
	{
		if (validationResult == null)
		{
			return new HashMap<>();
		}

		var game = validationResult.game;

		var diagnosticMap = convert(validationResult.problems);

		if (!validationResult.hasErrors && outputFolder != null)
		{
			generator.generate(game, outputFolder);
		}

		return diagnosticMap;
	}

	private Map<String,List<Diagnostic>> convert(Map<String,List<Problem>> problemMap)
	{
		var diagnosticMap = new HashMap<String, List<Diagnostic>>();

		if (problemMap == null)
		{
			return diagnosticMap;
		}

		problemMap.forEach((file, problems) ->
		{
			var diagnostics = new ArrayList<Diagnostic>();
			for (var problem : problems)
			{
				DiagnosticSeverity severity;
				switch(problem.severity)
				{
					case INFORMATION: severity = DiagnosticSeverity.Information; break;
					case WARNING: severity = DiagnosticSeverity.Warning; break;
					case ERROR: severity = DiagnosticSeverity.Error; break;
					default: severity = DiagnosticSeverity.Error; break;
				}
				var range = getRange(problem.node);
				var diagnostic = new Diagnostic(range, problem.message);
				diagnostic.setSeverity(severity);
				diagnostics.add(diagnostic);
			}

			diagnosticMap.put(file, diagnostics);
		});

		return diagnosticMap;
	}

	private Range getRange(INode node)
	{
		var text = node.getRootNode().getText();
		return new Range
		(
			new Position(node.getStartLine()-1, character(text, node.getOffset())),
			new Position(node.getEndLine()-1  , character(text, node.getOffset()+node.getLength()))
		);
	}

	private int character(String text, int offset)
	{
		var count = 0;
		for (var i = 0; i < offset && i < text.length(); i++)
		{
			count++;
			if (text.charAt(i) == '\n')
			{
				count = 0;
			}
		}
		return count;
	}
}
