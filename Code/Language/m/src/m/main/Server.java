package m.main;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.eclipse.lsp4j.CompletionItem;
import org.eclipse.lsp4j.CompletionList;
import org.eclipse.lsp4j.CompletionOptions;
import org.eclipse.lsp4j.CompletionParams;
import org.eclipse.lsp4j.ConfigurationItem;
import org.eclipse.lsp4j.ConfigurationParams;
import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.DidChangeConfigurationParams;
import org.eclipse.lsp4j.DidChangeTextDocumentParams;
import org.eclipse.lsp4j.DidChangeWatchedFilesParams;
import org.eclipse.lsp4j.DidChangeWorkspaceFoldersParams;
import org.eclipse.lsp4j.DidCloseTextDocumentParams;
import org.eclipse.lsp4j.DidOpenTextDocumentParams;
import org.eclipse.lsp4j.DidSaveTextDocumentParams;
import org.eclipse.lsp4j.DocumentFormattingParams;
import org.eclipse.lsp4j.DocumentSymbol;
import org.eclipse.lsp4j.DocumentSymbolParams;
import org.eclipse.lsp4j.FileChangeType;
import org.eclipse.lsp4j.Hover;
import org.eclipse.lsp4j.HoverParams;
import org.eclipse.lsp4j.InitializeParams;
import org.eclipse.lsp4j.InitializeResult;
import org.eclipse.lsp4j.InitializedParams;
import org.eclipse.lsp4j.Location;
import org.eclipse.lsp4j.MarkupContent;
import org.eclipse.lsp4j.MessageParams;
import org.eclipse.lsp4j.MessageType;
import org.eclipse.lsp4j.PublishDiagnosticsParams;
import org.eclipse.lsp4j.ReferenceParams;
import org.eclipse.lsp4j.RenameParams;
import org.eclipse.lsp4j.SemanticTokens;
import org.eclipse.lsp4j.SemanticTokensLegend;
import org.eclipse.lsp4j.SemanticTokensParams;
import org.eclipse.lsp4j.SemanticTokensServerFull;
import org.eclipse.lsp4j.SemanticTokensWithRegistrationOptions;
import org.eclipse.lsp4j.ServerCapabilities;
import org.eclipse.lsp4j.SignatureHelp;
import org.eclipse.lsp4j.SignatureHelpOptions;
import org.eclipse.lsp4j.SignatureHelpParams;
import org.eclipse.lsp4j.SymbolInformation;
import org.eclipse.lsp4j.TextDocumentSyncKind;
import org.eclipse.lsp4j.TextEdit;
import org.eclipse.lsp4j.WorkspaceEdit;
import org.eclipse.lsp4j.WorkspaceFoldersOptions;
import org.eclipse.lsp4j.WorkspaceServerCapabilities;
import org.eclipse.lsp4j.WorkspaceSymbolParams;
import org.eclipse.lsp4j.jsonrpc.messages.Either;
import org.eclipse.lsp4j.launch.LSPLauncher;
import org.eclipse.lsp4j.services.LanguageClient;
import org.eclipse.lsp4j.services.LanguageServer;
import org.eclipse.lsp4j.services.TextDocumentService;
import org.eclipse.lsp4j.services.WorkspaceService;

import m.generator.IO;

public class Server implements LanguageServer, WorkspaceService, TextDocumentService
{
	LanguageClient client;
	Map<String,Project> projects;

	@Override
	public WorkspaceService getWorkspaceService()
	{
		return this;
	}

	@Override
	public TextDocumentService getTextDocumentService()
	{
		return this;
	}


	public void connect(InputStream input, OutputStream output)
	{
		var connection = LSPLauncher.createServerLauncher(this, input, output);
		client = connection.getRemoteProxy();
		connection.startListening();
	}

	@Override
	public CompletableFuture<InitializeResult> initialize(InitializeParams params)
	{
		this.projects = new HashMap<>();

		var options = new WorkspaceFoldersOptions();
		options.setSupported(true);
		options.setChangeNotifications(true);

		var semanticOptions = new SemanticTokensWithRegistrationOptions();

		var tokenTypes = new ArrayList<String>();
		tokenTypes.add("class");
		tokenTypes.add("function");
		tokenTypes.add("keyword");
		tokenTypes.add("operator");
		tokenTypes.add("parameter");
		tokenTypes.add("variable");
		tokenTypes.add("property");

		var tokenModifiers = new ArrayList<String>();
		tokenModifiers.add("defaultLibrary");
		tokenModifiers.add("readonly");
		tokenModifiers.add("deprecated");

		semanticOptions.setFull(new SemanticTokensServerFull(true));
		semanticOptions.setLegend(new SemanticTokensLegend(tokenTypes, tokenModifiers));

		var capabilities = new ServerCapabilities();
		capabilities.setTextDocumentSync(TextDocumentSyncKind.Full);
		capabilities.setWorkspace(new WorkspaceServerCapabilities(options));
		capabilities.setHoverProvider(true);
		capabilities.setCompletionProvider(new CompletionOptions(false, Arrays.asList(".")));
		capabilities.setSignatureHelpProvider(new SignatureHelpOptions(Arrays.asList("(", ",")));
		capabilities.setDocumentFormattingProvider(true);
		capabilities.setDocumentSymbolProvider(true);
		capabilities.setWorkspaceSymbolProvider(true);
		capabilities.setRenameProvider(true);
		capabilities.setSemanticTokensProvider(semanticOptions);
		capabilities.setReferencesProvider(true);

		for (var folder : params.getWorkspaceFolders())
		{
			var uri = folder.getUri();
			projects.put(uri, new Project(uri));
		}

		return CompletableFuture.supplyAsync(() -> new InitializeResult(capabilities));
	}



	@Override
	public void initialized(InitializedParams params)
	{
		var items = new ArrayList<ConfigurationItem>();
		var item = new ConfigurationItem();
		item.setSection("m.outputFolder");
		items.add(item);
		client.configuration(new ConfigurationParams(items)).thenAccept(s ->
		{
			for ( var project : projects.values())
			{
				project.setConfiguration(s);
			}
		});

		for (var project : projects.values())
		{
			var diagnostics = project.initialize();
			publishDiagnostics(diagnostics);
		}
	}

	@Override
	public void didChangeConfiguration(DidChangeConfigurationParams params)
	{
	}

	@Override
	public CompletableFuture<Object> shutdown()
	{
		return CompletableFuture.supplyAsync(() -> Boolean.TRUE);
	}

	@Override
	public void exit()
	{
		System.exit(0);
	}




	@Override
	public void didChangeWorkspaceFolders(DidChangeWorkspaceFoldersParams params)
	{
		for (var added : params.getEvent().getAdded())
		{
			var uri = added.getUri();
			var project = new Project(uri);
			projects.put(uri,project);

			var diagnostics = project.initialize();
			publishDiagnostics(diagnostics);
		}
		for (var removed : params.getEvent().getRemoved())
		{
			var uri = removed.getUri();

			projects.remove(uri);
		}
	}

	@Override
	public void didChangeWatchedFiles(DidChangeWatchedFilesParams params)
	{
		for (var change : params.getChanges())
		{
			var file = change.getUri();

			if (change.getType() == FileChangeType.Created)
			{
				var text = IO.read(file);

				for (var project : projectsContaining(file))
				{
					var diagnostics = project.modify(file, text);
					publishDiagnostics(diagnostics);
				}
			}
			else if (change.getType() == FileChangeType.Deleted)
			{
				for (var project : projectsContaining(file))
				{
					var diagnostics = project.delete(file);
					publishDiagnostics(diagnostics);
				}
			}
		}
	}


	@Override
	public void didOpen(DidOpenTextDocumentParams params)
	{
		modify(params.getTextDocument().getUri(), params.getTextDocument().getText());
	}

	@Override
	public void didChange(DidChangeTextDocumentParams params)
	{
		modify(params.getTextDocument().getUri(), params.getContentChanges().get(0).getText());
	}

	@Override
	public void didClose(DidCloseTextDocumentParams params)
	{

	}

	@Override
	public void didSave(DidSaveTextDocumentParams params)
	{

	}





	@Override
	public CompletableFuture<Hover> hover(HoverParams params)
	{
		var position = params.getPosition();
		var file = params.getTextDocument().getUri();

		var result = "";

		for (var project : projectsContaining(file))
		{
			result += project.hover(file, position);
		}

		var hover = new Hover();
		var contents = new MarkupContent("markdown", result);
		hover.setContents(contents);
		return CompletableFuture.supplyAsync(() -> hover);
	}

	@Override
	public CompletableFuture<Either<List<CompletionItem>, CompletionList>> completion(CompletionParams params)
	{
		var file = params.getTextDocument().getUri();
		var position = params.getPosition();

		var result = new ArrayList<CompletionItem>();

		for (var project : projectsContaining(file))
		{
			result.addAll(project.completions(file, position));
		}

		return CompletableFuture.supplyAsync(() -> Either.forLeft(result));
	}

	@Override
	public CompletableFuture<SignatureHelp> signatureHelp(SignatureHelpParams params)
	{
		var file = params.getTextDocument().getUri();

		var result = new SignatureHelp();

		for (var project : projectsContaining(file))
		{
			result = project.signatures(file, params.getPosition(), params.getContext().getTriggerCharacter());
		}

		var finalResult = result;

		return CompletableFuture.supplyAsync(() -> finalResult);
	}

	@Override
	public CompletableFuture<List<? extends TextEdit>> formatting(DocumentFormattingParams params)
	{
		var file = params.getTextDocument().getUri();

		var result = new ArrayList<TextEdit>();

		for (var project : projectsContaining(file))
		{
			result.add(project.format(file));
		}

		var finalResult = result;

		return CompletableFuture.supplyAsync(() -> finalResult);
	}

	@Override
	public CompletableFuture<List<Either<SymbolInformation, DocumentSymbol>>> documentSymbol(DocumentSymbolParams params)
	{
		var file = params.getTextDocument().getUri();

		var result = new ArrayList<Either<SymbolInformation, DocumentSymbol>>();

		for (var project : projectsContaining(file))
		{
			for (var symbol : project.symbols(file))
			{
				result.add(Either.forLeft(symbol));
			}
		}

		return CompletableFuture.supplyAsync(() -> result);
	}

	public CompletableFuture<List<? extends SymbolInformation>> symbol(WorkspaceSymbolParams params)
	{
		var result = new ArrayList<SymbolInformation>();

		for (var project : projects.values())
		{
			result.addAll(project.symbols());
		}

		return CompletableFuture.supplyAsync(() -> result);
	}

	@Override
	public CompletableFuture<WorkspaceEdit> rename(RenameParams params)
	{
		var result = new WorkspaceEdit();

		var file = params.getTextDocument().getUri();

		for (var project : projectsContaining(file))
		{
			result = project.rename(file,params.getPosition(),params.getNewName());
		}

		var finalResult = result;

		return CompletableFuture.supplyAsync(() -> finalResult);
	}

	@Override
	public CompletableFuture<SemanticTokens> semanticTokensFull(SemanticTokensParams params)
	{
		var items = new ArrayList<Integer>();

		var result = new SemanticTokens(items);

		var file = params.getTextDocument().getUri();

		for (var project : projectsContaining(file))
		{
			result = project.semanticTokens(file);
		}

		var finalResult = result;

		return CompletableFuture.supplyAsync(() -> finalResult);
	}

	@Override
	public CompletableFuture<List<? extends Location>> references(ReferenceParams params)
	{
		var result = new ArrayList<Location>();

		var file = params.getTextDocument().getUri();

		for (var project : projectsContaining(file))
		{
			result.addAll(project.references(file,params.getPosition()));
		}

		var finalResult = result;

		return CompletableFuture.supplyAsync(() -> finalResult);
	}

	private List<Project> projectsContaining(String file)
	{
		var result = new ArrayList<Project>();

		projects.forEach((folder,project) ->
		{
			if (IO.contains(folder,file))
			{
				result.add(project);
			}
		});

		if (result.isEmpty())
		{
			client.showMessage(new MessageParams(MessageType.Info, "Ⲙ file detected outside of workspace folders. Open the file in a workspace folder."));
		}

		return result;
	}



	private void modify(String file, String text)
	{
		for (var project : projectsContaining(file))
		{
			var diagnostics = project.modify(file, text);
			publishDiagnostics(diagnostics);
		}
	}

	private void publishDiagnostics(Map<String,List<Diagnostic>> diagnosticMap)
	{
		diagnosticMap.forEach((uri, diagnostics)->
		{
			var parameters = new PublishDiagnosticsParams(uri, diagnostics);
			client.publishDiagnostics(parameters);
		});
	}
}
