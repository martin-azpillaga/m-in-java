package m.model;

import m.model.impl.FunctionImpl;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.xtext.EcoreUtil2;

import m.library.types.FunctionType;

public class UserFunction extends FunctionImpl
{
	public final FunctionType type;
	public Map<String,HashMap<String,Boolean>> roles;
	public final Function function;

	public UserFunction(Function function, FunctionType type)
	{
		this.function = function;
		this.name = function.getName();
		this.statements = function.getStatements();
		this.type = type;
		this.roles = collectQueries();
	}

	public HashMap<String,HashMap<String,Boolean>> collectQueries()
	{
		var result = new HashMap<String,HashMap<String,Boolean>>();

		for (var binding : EcoreUtil2.getAllContentsOfType(this, BindingBlock.class))
		{
			var entity = binding.getExpression().getName();

			var isHit = false;

			for (var binaryExpression : EcoreUtil2.getAllContentsOfType(binding, Binary.class))
			{
				if (binaryExpression.getOperator().equals("@@") && (binaryExpression.getLeft() instanceof Value) && ((Value)binaryExpression.getLeft()).getName().equals(entity))
				{
					isHit = true;
				}
			}

			if (!result.containsKey(entity) && !isHit)
			{
				result.put(entity, new HashMap<>());
			}
		}

		for (var cell : EcoreUtil2.getAllContentsOfType(this,Cell.class))
		{
			var name = cell.getEntity().getName();
			if (cell.getComponent() == null)
			{
				continue;
			}

			var component = cell.getComponent().getName();
			var container = cell.eContainer();

			if (container instanceof Assignment)
			{
				var assignment = (Assignment) container;
				setComponentAccess(result, name, component, cell == assignment.getAtom());
			}
			else
			{
				setComponentAccess(result, name, component, false);
			}
		}


		return result;
	}

	private void setComponentAccess(HashMap<String,HashMap<String,Boolean>> result, String entity, String component, boolean writeAccess)
	{
		if (result.containsKey(entity))
		{
			var components = result.get(entity);
			if (!components.containsKey(component) || !components.get(component))
			{
				result.get(entity).put(component, writeAccess);
			}
		}
	}
}
