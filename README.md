# M

A data-oriented programming language for game developers.

## Installation

Install the **M language server** extension from a `vscode` marketplace.

## Documentation

Read the online documentation at [minim.tools/M](https://minim.tools/M).
